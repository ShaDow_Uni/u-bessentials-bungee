package shadowuni.plugin.b.essentials.api;

import java.util.Calendar;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import shadowuni.plugin.b.essentials.api.manager.BanManager;
import shadowuni.plugin.b.essentials.api.manager.ChannelManager;
import shadowuni.plugin.b.essentials.api.manager.ChatManager;
import shadowuni.plugin.b.essentials.api.manager.FileManager;
import shadowuni.plugin.b.essentials.api.manager.LobbyManager;
import shadowuni.plugin.b.essentials.api.manager.PlayerManager;
import shadowuni.plugin.b.essentials.api.manager.SQLManager;
import shadowuni.plugin.b.essentials.api.manager.WarningEventManager;
import shadowuni.plugin.b.essentials.api.manager.WarningManager;
import shadowuni.plugin.bungeelogincore.BungeeLoginCoreAPI;

@Getter
public class EssentialsAPI {
	
	private final String pluginPrefix = ChatColor.GRAY + "[ U-BEssentials ] " + ChatColor.WHITE;
	
	@Setter
	private String prefix, kickMark, warningMessage, warningDisplayname;
	
	@Setter
	private String SQLAddress, SQLDatabase, SQLUser, SQLPassword, SQLTablePrefix;
	@Setter
	private int SQLPort;
	
	@Setter
	private int kickWarningCount, banWarningCount;
	
	@Setter
	private boolean useMySQL, useLobby, useChatFilter, useWarning, ignoreFilterWordNickname, sendToLobbyOnConnect, loadAllPlayerDataOnStart;
	@Setter
	private boolean useBungeeLogin;
	
	private PlayerManager playerManager;
	private BanManager banManager;
	private ChatManager chatManager;
	private LobbyManager lobbyManager;
	private WarningManager warningManager;
	private WarningEventManager warningEventManager;
	private ChannelManager channelManager;
	private FileManager fileManager;
	private SQLManager SQLManager;
	@Setter
	private BungeeLoginCoreAPI loginAPI;
	
	public void init() {
		playerManager = new PlayerManager();
		banManager = new BanManager();
		chatManager = new ChatManager();
		lobbyManager = new LobbyManager();
		warningManager = new WarningManager();
		warningEventManager = new WarningEventManager();
		channelManager = new ChannelManager();
		fileManager = new FileManager();
		SQLManager = new SQLManager();
	}
	
	public void log(String log) {
		nlog(getPluginPrefix() + log);
	}
	
	public void nlog(String log) {
		ProxyServer.getInstance().getConsole().sendMessage(log);
	}
	
	public void msg(CommandSender sender, String msg) {
		nmsg(sender, getPluginPrefix() + msg);
	}
	
	public void nmsg(CommandSender sender, String msg) {
		sender.sendMessage(msg);
	}
	
	public void broadcast(String msg) {
		nbroadcast(getPrefix() + msg);
	}
	
	public void nbroadcast(String msg) {
		ProxyServer.getInstance().broadcast(msg);
	}
	
	public String buildTime(long time) {
		StringBuilder sb = new StringBuilder();
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time);
		String[] times = new String[6];
		times[0] = c.get(Calendar.YEAR) + "년";
		times[1]  = (c.get(Calendar.MONTH) + 1) + "월";
		times[2] = c.get(Calendar.DAY_OF_MONTH) + "일";
		times[3] = c.get(Calendar.HOUR_OF_DAY) > 0 ? c.get(Calendar.HOUR_OF_DAY) + "시" : null;
		times[4] = c.get(Calendar.MINUTE) > 0 ? c.get(Calendar.MINUTE) + "분" : null;
		times[5] = c.get(Calendar.SECOND) > 0 ? c.get(Calendar.SECOND) + "초" : null;
		for(int i = 0; i < 6; i++) {
			String sTime = times[i];
			if(sTime == null) continue;
			sb.append(sb.length() == 0 ? sTime : " " + sTime);
		}
		return sb.toString();
	}
	
	public boolean permCheck(ProxiedPlayer p, String perm) {
		if(p.hasPermission(perm)) return true;
		msg(p, "권한이 없습니다!");
		return false;
	}
	
}

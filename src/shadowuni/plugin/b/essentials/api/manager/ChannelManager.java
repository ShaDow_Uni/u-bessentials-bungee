package shadowuni.plugin.b.essentials.api.manager;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.b.essentials.api.object.Channel;

public class ChannelManager {
	
	@Setter
	@Getter
	private HashMap<String, Channel> channels = new HashMap<>();
	
	public void setChannel(String name, Channel channel) {
		channels.put(name.toLowerCase(), channel);
	}
	
	public boolean existChannel(String name) {
		return channels.containsKey(name.toLowerCase());
	}
	
	public Channel getChannel(String name) {
		if(!existChannel(name)) return null;
		return channels.get(name.toLowerCase());
	}
	
}

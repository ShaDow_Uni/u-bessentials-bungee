package shadowuni.plugin.b.essentials.api.manager;

import java.io.File;
import java.io.IOException;

import lombok.Getter;
import net.craftminecraft.bungee.bungeeyaml.bukkitapi.file.FileConfiguration;
import net.craftminecraft.bungee.bungeeyaml.bukkitapi.file.YamlConfiguration;
import net.craftminecraft.bungee.bungeeyaml.pluginapi.ConfigurablePlugin;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.category.Event;
import shadowuni.plugin.b.essentials.api.category.ListeningChannel;
import shadowuni.plugin.b.essentials.api.object.Channel;
import shadowuni.plugin.b.essentials.api.object.WarningEvent;

public class FileManager extends ConfigurablePlugin {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@Getter
	private File configFile = new File("plugins/U-BEssentials/config.yml");
	@Getter
	private File chatFilterConfigFile = new File("plugins/U-BEssentials/chat-filter.yml");
	@Getter
	private File channelFile = new File("plugins/U-BEssentials/channel.yml");
	@Getter
	private FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);
	@Getter
	private FileConfiguration chatFilterConfig = YamlConfiguration.loadConfiguration(chatFilterConfigFile);
	@Getter
	private FileConfiguration channelConfig = YamlConfiguration.loadConfiguration(channelFile);
	
	public void createConfigFile() {
		if(!configFile.exists()) {
			config = YamlConfiguration.loadConfiguration(getClass().getClassLoader().getResourceAsStream("config.yml"));
			try {
				config.save(configFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createChatFilterFile() {
		if(!chatFilterConfigFile.exists()) {
			chatFilterConfig = YamlConfiguration.loadConfiguration(getClass().getClassLoader().getResourceAsStream("chatfilter.yml"));
			try {
				chatFilterConfig.save(chatFilterConfigFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createChannelFile() {
		if(!channelFile.exists()) {
			try {
				channelFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void loadConfig() {
		createConfigFile();
		config = YamlConfiguration.loadConfiguration(configFile);
		api.setPrefix(ChatColor.translateAlternateColorCodes('&', config.getString("접두사")));
		api.setKickMark(ChatColor.translateAlternateColorCodes('&', config.getString("강제 퇴장 서버 이름")));
		api.setUseLobby(config.getBoolean("로비 사용"));
		api.getLobbyManager().setLobbyList(config.getStringList("로비 목록"));
		api.setSendToLobbyOnConnect(config.getBoolean("접속 시 로비로 이동"));
		if(api.isUseLobby()) {
			if(ProxyServer.getInstance().getPluginManager().getPlugin("U-BungeeInfoCore") == null) {
				api.log("U-BungeeInfoCore 플러그인을 찾을 수 없어 로비 사용을 비활성화합니다.");
				api.setUseLobby(false);
			}
		}
		api.setLoadAllPlayerDataOnStart(config.getBoolean("MySQL.시작 시 모든 유저 데이터 불러오기"));
		api.setSQLAddress(config.getString("MySQL.주소"));
		api.setSQLPort(config.getInt("MySQL.포트"));
		api.setSQLDatabase(config.getString("MySQL.데이터베이스"));
		api.setSQLUser(config.getString("MySQL.유저"));
		api.setSQLPassword(config.getString("MySQL.비밀번호"));
		api.setSQLTablePrefix(config.getString("MySQL.접두사"));
		api.log("설정을 불러왔습니다!");
	}
	
	public void loadChatFilterConfig() {
		createChatFilterFile();
		api.setUseChatFilter(chatFilterConfig.getBoolean("채팅 필터.사용"));
		api.setIgnoreFilterWordNickname(chatFilterConfig.getBoolean("채팅 필터.필터 단어 포함 닉네임 채팅 무시"));
		api.getChatManager().getBanWords().clear();
		for(String line : chatFilterConfig.getStringList("채팅 필터.필터 단어")) {
			String word = line;
			int wCount = 1;
			if(line.contains(" ")) {
				String[] lines = line.split(" ");
				word = lines[0];
				wCount = Integer.parseInt(lines[1]);
			}
			api.getChatManager().getBanWords().put(word, wCount);
		}
		api.setUseWarning(chatFilterConfig.getBoolean("경고.사용"));
		api.setWarningDisplayname(chatFilterConfig.getString("경고.처리자 표시"));
		api.setWarningMessage(ChatColor.translateAlternateColorCodes('&', chatFilterConfig.getString("경고.메시지")));
		api.getWarningEventManager().getEvents().clear();
		for(String event : chatFilterConfig.getStringList("경고.이벤트")) {
			String tasks[] = event.split(" ");
			int count = Integer.parseInt(tasks[0]);
			String task = tasks[1];
			WarningEvent e = new WarningEvent();
			if(task.equalsIgnoreCase("kick")) {
				e.setType(Event.KICK);
				StringBuilder sb = new StringBuilder();
				for(int i = 2; i < tasks.length; i++) {
					if(sb.length() < 1) {
						sb.append(tasks[i]);
					} else {
						sb.append(" " + tasks[i]);
					}
				}
				e.setMessage(sb.toString());
			} else if(task.equalsIgnoreCase("ban")) {
				e.setType(Event.BAN);
				StringBuilder sb = new StringBuilder();
				for(int i = 2; i < tasks.length; i++) {
					if(sb.length() < 1) {
						sb.append(tasks[i]);
					} else {
						sb.append(" " + tasks[i]);
					}
				}
				e.setMessage(sb.toString());
			} else if(task.equalsIgnoreCase("tempban")) {
				int d = 0, h = 0, m = 0, s = 0, l = 0;
				for(int i = 2; i < 6; i++) {
					if(tasks.length < i + 1 || tasks[i].length() < 3)  break;
					if(tasks[i].substring(0, 2).equalsIgnoreCase("d:")) {
						d = Integer.parseInt(tasks[i].substring(2, tasks[i].length())); l++;
					} else if(tasks[i].substring(0, 2).equalsIgnoreCase("h:")) {
						h = Integer.parseInt(tasks[i].substring(2, tasks[i].length())); l++;
					} else if(tasks[i].substring(0, 2).equalsIgnoreCase("m:")) {
						m = Integer.parseInt(tasks[i].substring(2, tasks[i].length())); l++;
					} else if(tasks[i].substring(0, 2).equalsIgnoreCase("s:")) {
						s = Integer.parseInt(tasks[i].substring(2, tasks[i].length())); l++;
					}
				}
				StringBuilder sb = new StringBuilder();
				for(int i = l + 2; i < tasks.length; i++) {
					if(sb.length() < 1) {
						sb.append(tasks[i]);
					} else {
						sb.append(" " + tasks[i]);
					}
				}
				e.setTime((d * 86400 + h * 3600 + m * 60 + s) * 1000);
				e.setMessage(ChatColor.translateAlternateColorCodes('&', sb.toString()));
				e.setType(Event.TEMPBAN);
			} else if(task.equalsIgnoreCase("ipban")) {
				e.setType(Event.IPBAN);
				StringBuilder sb = new StringBuilder();
				for(int i = 2; i < tasks.length; i++) {
					if(sb.length() < 1) {
						sb.append(tasks[i]);
					} else {
						sb.append(" " + tasks[i]);
					}
				}
				e.setMessage(sb.toString());
			} else if(task.equalsIgnoreCase("tempipban")) {
				int d = 0, h = 0, m = 0, s = 0, l = 0;
				for(int i = 2; i < 6; i++) {
					if(tasks.length < i + 1 || tasks[i].length() < 3)  break;
					if(tasks[i].substring(0, 2).equalsIgnoreCase("d:")) {
						d = Integer.parseInt(tasks[i].substring(2, tasks[i].length())); l++;
					} else if(tasks[i].substring(0, 2).equalsIgnoreCase("h:")) {
						h = Integer.parseInt(tasks[i].substring(2, tasks[i].length())); l++;
					} else if(tasks[i].substring(0, 2).equalsIgnoreCase("m:")) {
						m = Integer.parseInt(tasks[i].substring(2, tasks[i].length())); l++;
					} else if(tasks[i].substring(0, 2).equalsIgnoreCase("s:")) {
						s = Integer.parseInt(tasks[i].substring(2, tasks[i].length())); l++;
					}
				}
				StringBuilder sb = new StringBuilder();
				for(int i = l + 2; i < tasks.length; i++) {
					if(sb.length() < 1) {
						sb.append(tasks[i]);
					} else {
						sb.append(" " + tasks[i]);
					}
				}
				e.setTime((d * 86400 + h * 3600 + m * 60 + s) * 1000);
				e.setMessage(ChatColor.translateAlternateColorCodes('&', sb.toString()));
				e.setType(Event.TEMPIPBAN);
			}
			api.getWarningEventManager().setEvent(count, e);
		}
	}
	
	public void loadChannelConfig() {
		createChannelFile();
		channelConfig = YamlConfiguration.loadConfiguration(channelFile);
		api.getChannelManager().getChannels().clear();
		for(String channelName : ProxyServer.getInstance().getServers().keySet()) {
			channelName = channelName.replace(".", "@");
			if(channelConfig.getString(channelName) == null) {
				channelConfig.set(channelName + ".가상 이름", channelName.toUpperCase().charAt(0));
				channelConfig.set(channelName + ".듣기 채널", "local");
				channelConfig.set(channelName + ".채팅 양식", "&7[{channel}] &f{prefixerprefix}&f{rankprefix}&f{name}&f : {chat}");
				try {
					channelConfig.save(channelFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			Channel channel = new Channel();
			channel.setName(channelName.replace("@", "."));
			if(channelConfig.getString(channelName + ".가상 이름") != null) {
				channel.setDisplayName(channelConfig.getString(channelName + ".가상 이름"));
			}
			channel.setListeningChannel(ListeningChannel.LOCAL);
			if(channelConfig.getString(channelName + ".듣기 채널") != null) {
				if(channelConfig.getString(channelName + ".듣기 채널").equalsIgnoreCase("global")) {
					channel.setListeningChannel(ListeningChannel.GLOBAL);
				}
			}
			if(channelConfig.getString(channelName + ".채팅 양식") != null) {
				channel.setChatForm(ChatColor.translateAlternateColorCodes('&', channelConfig.getString(channelName + ".채팅 양식").replace("{channel}", channel.getDisplayName())));
			} else {
				channel.setChatForm(ChatColor.GRAY + "[" + channel.getDisplayName() + "] " + ChatColor.WHITE + "{prefixerprefix}" + ChatColor.WHITE + "{rankprefix}" + ChatColor.WHITE + "{name}" + ChatColor.WHITE + " : {chat}");
			}
			api.getChannelManager().setChannel(channel.getName(), channel);
		}
	}

}

package shadowuni.plugin.b.essentials.api.manager;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class WarningManager {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	@Getter
	private Timer initTimer;
	@Getter
	private HashMap<String, Integer> warnings = new HashMap<>(); // <Player, Warning>
	
	public boolean existsWarning(ProxiedPlayer p) {
		return existsWarning(p.getName());
	}
	
	public boolean existsWarning(String name) {
		return warnings.containsKey(name.toLowerCase());
	}
	
	public Integer getWarning(ProxiedPlayer p) {
		return getWarning(p.getName());
	}
	
	public Integer getWarning(String name) {
		if(!existsWarning(name)) return 0;
		return warnings.get(name.toLowerCase());
	}
	
	public void setWarning(ProxiedPlayer p, int count) {
		setWarning(p.getName(), count);
	}
	
	public void setWarning(String name, int count) {
		warnings.put(name.toLowerCase(), count);
		api.getSQLManager().saveWarning(name);
	}
	
	public void giveWarning(ProxiedPlayer p, int count) {
		giveWarning(p.getName(), count);
	}
	
	public void giveWarning(String name, int count) {
		setWarning(name, getWarning(name) + count);
	}

	public String buildTime() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.YEAR) + "@" + c.get(Calendar.MONTH) + 1 + "@" + c.get(Calendar.DATE);
	}
	
	public void startInitTimer() {
		if(initTimer != null) return;
		initTimer = new Timer();
		Calendar c = Calendar.getInstance();
		c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE) + 1, 0, 0, 0);
		Date date = c.getTime();
		initTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				api.getWarningManager().warnings.clear();
				api.getSQLManager().initWarning();
				api.log("경고가 초기화되었습니다.");
			}
		}, date, 86400000);
	}
	
	public void stopInitTimer() {
		if(initTimer == null) return;
		initTimer.cancel();
		initTimer = null;
	}

}

package shadowuni.plugin.b.essentials.api.manager;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BanData;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;
import shadowuni.plugin.b.essentials.api.object.IpBanData;

public class BanManager {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@Setter
	@Getter
	private HashMap<String, BanData> banDatas = new HashMap<>();
	@Setter
	@Getter
	private HashMap<String, IpBanData> IpBanDatas = new HashMap<>();
	
	public boolean kickPlayer(ProxiedPlayer p, String reason, String kickedBy) {
		if(p == null || (api.getPlayerManager().isMasterPlayer(p.getName()) || api.getPlayerManager().isMasterIp(p.getAddress().getHostName()))) return false;
		p.disconnect(reason + "\n[처리자: " + kickedBy + "]\n\n\n\n\n" + api.getKickMark());
		return true;
	}
	
	public boolean kickPlayer(String name, String reason, String bannedBy) {
		return kickPlayer(ProxyServer.getInstance().getPlayer(name), reason, bannedBy);
	}
	
	public void setBanData(String name, BanData bd) {
		getBanDatas().put(name.toLowerCase(), bd);
	}
	
	public boolean existsBanData(ProxiedPlayer p) {
		return existsBanData(p.getName());
	}
	
	public boolean existsBanData(BungeePlayer bp) {
		return existsBanData(bp.getName());
	}
	
	public boolean existsBanData(String name) {
		return getBanDatas().containsKey(name.toLowerCase());
	}
	
	public BanData getBanData(ProxiedPlayer p) {
		return getBanData(p.getName());
	}
	
	public BanData getBanData(BungeePlayer bp) {
		return getBanData(bp.getName());
	}
	
	public BanData getBanData(String name) {
		return getBanDatas().get(name.toLowerCase());
	}
	
	public void removeBanData(ProxiedPlayer p) {
		removeBanData(p.getName());
	}
	
	public void removeBanData(BungeePlayer bp) {
		removeBanData(bp.getName());;
	}
	
	public void removeBanData(String name) {
		getBanDatas().remove(name.toLowerCase());
	}
	
	public void setIpBanData(String ip, IpBanData bd) {
		getIpBanDatas().put(ip, bd);
	}
	
	public boolean existsIpBanData(ProxiedPlayer p) {
		return existsBanData(p.getAddress().getHostName());
	}
	
	public boolean existsIpBanData(BungeePlayer bp) {
		return existsBanData(bp.getIp());
	}
	
	public boolean existsIpBanData(String ip) {
		return getIpBanDatas().containsKey(ip);
	}
	
	public IpBanData getIpBanData(ProxiedPlayer p) {
		return getIpBanData(p.getAddress().getHostName());
	}
	
	public IpBanData getIpBanData(BungeePlayer bp) {
		return getIpBanData(bp.getIp());
	}
	
	public IpBanData getIpBanData(String ip) {
		return IpBanDatas.get(ip);
	}
	
	public void removeIpBanData(ProxiedPlayer p) {
		removeBanData(p.getAddress().getHostName());
	}
	
	public void removeIpBanData(BungeePlayer bp) {
		removeBanData(bp.getIp());
	}
	
	public void removeIpBanData(String ip) {
		IpBanDatas.remove(ip);
	}
	
	public boolean isBannedPlayer(ProxiedPlayer p) {
		return isBannedPlayer(p.getName());
	}
	
	public boolean isBannedPlayer(BungeePlayer bp) {
		return isBannedPlayer(bp.getName());
	}
	
	public boolean isBannedPlayer(String name) {
		BanData bd = getBanData(name);
		if(bd == null) return false;
		return bd.isTempBan() && bd.isBanTimePassed();
	}
	
	public boolean isIpBannedPlayer(ProxiedPlayer p) {
		return isBannedIp(p.getAddress().getHostName());
	}
	
	public boolean isIpBannedPlayer(BungeePlayer bp) {
		return isBannedIp(bp.getIp());
	}
	
	public boolean isIpBannedPlayer(String name) {
		if(!api.getPlayerManager().existsBungeePlayer(name)) return false;
		return isIpBannedPlayer(api.getPlayerManager().getBungeePlayer(name));
	}
	
	public boolean isBannedIp(String ip) {
		IpBanData bd = getIpBanData(ip);
		if(bd == null) return false;
		return bd.isTempBan() && bd.isBanTimePassed();
	}
	
	public boolean banPlayer(ProxiedPlayer p, String reason, String bannedBy, long banTime) {
		return banPlayer(p.getName(), reason, bannedBy, banTime);
	}
	
	public boolean banPlayer(BungeePlayer bp, String reason, String bannedBy, long banTime) {
		return banPlayer(bp.getName(), reason, bannedBy, banTime);
	}
	
	public boolean banPlayer(String name, String reason, String bannedBy, long banTime) {
		if(isBannedPlayer(name)) return false;
		BanData bd = new BanData(name, bannedBy, reason, System.currentTimeMillis(), banTime);
		setBanData(name, bd);
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> {
			api.getSQLManager().saveBanData(bd);
			api.getSQLManager().writeBanLog(bd);
		});
		return true;
	}
	
	public boolean banPlayerIp(ProxiedPlayer p, String reason, String bannedBy, long banTime) {
		return banIp(p.getAddress().getHostName(), reason, bannedBy, banTime);
	}
	
	public boolean banPlayerIp(BungeePlayer bp, String reason, String bannedBy, long banTime) {
		return banIp(bp.getIp(), reason, bannedBy, banTime);
	}
	
	public boolean banPlayerIp(String name, String reason, String bannedBy, long banTime) {
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(name);
		if(bp == null) return false;
		return banPlayerIp(bp, reason, bannedBy, banTime);
	}
	
	public boolean banIp(String ip, String reason, String bannedBy, long banTime) {
		if(isBannedIp(ip)) return false;
		IpBanData bd = new IpBanData(ip, bannedBy, reason, System.currentTimeMillis(), banTime);
		setIpBanData(ip, bd);
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> {
			api.getSQLManager().saveIpBanData(bd);
			api.getSQLManager().writeIpBanLog(bd);
		});
		return true;
	}
	
	public boolean unBanPlayer(ProxiedPlayer p) {
		return unBanPlayer(p.getName());
	}
	
	public boolean unBanPlayer(BungeePlayer bp) {
		return unBanPlayer(bp.getName());
	}
	
	public boolean unBanPlayer(String name) {
		if(!existsBanData(name)) return false;
		removeBanData(name);
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> {
			api.getSQLManager().removeBanData(name);
		});
		return true;
	}
	
	public boolean unBanPlayerIp(ProxiedPlayer p) {
		return unBanIp(p.getAddress().getHostName());
	}
	
	public boolean unBanPlayerIp(BungeePlayer bp) {
		return unBanIp(bp.getName());
	}
	
	public boolean unBanPlayerIp(String name) {
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(name);
		if(bp == null) return false;
		return unBanIp(bp.getIp());
	}
	
	public boolean unBanIp(String ip) {
		removeIpBanData(ip);
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> {
			api.getSQLManager().removeIpBanData(ip);
		});
		return true;
	}
	
}
package shadowuni.plugin.b.essentials.api.manager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

@Getter
public class PlayerManager {
	
	private final List<String> masterNickNames;
	private final List<String> masterIps;
	
	@Setter
	private HashMap<String, BungeePlayer> bungeePlayers = new HashMap<>();
	
	public PlayerManager() {
		masterNickNames = loadMasterNickName();
		masterIps = loadMasterIp();
	}
	
	public void setBungeePlayer(String name, BungeePlayer bp) {
		getBungeePlayers().put(name.toLowerCase(), bp);
	}
	
	public void removeBungeePlayer(BungeePlayer bp) {
		removeBungeePlayer(bp.getName());
	}
	
	public void removeBungeePlayer(String name) {
		getBungeePlayers().remove(name.toLowerCase());
	}
	
	public boolean existsBungeePlayer(ProxiedPlayer p) {
		return existsBungeePlayer(p.getName());
	}
	
	public boolean existsBungeePlayer(String name) {
		return getBungeePlayers().containsKey(name.toLowerCase());
	}
	
	public BungeePlayer getBungeePlayer(ProxiedPlayer p) {
		return getBungeePlayer(p.getName());
	}
	
	public BungeePlayer getBungeePlayer(CommandSender sender) {
		return getBungeePlayer(sender.getName());
	}
	
	public BungeePlayer getBungeePlayer(String name) {
		return getBungeePlayers().get(name.toLowerCase());
	}
	
	public BungeePlayer getBungeePlayer(ProxiedPlayer p, boolean loadFromSQL) {
		return getBungeePlayer(p.getName(), loadFromSQL);
	}
	
	public BungeePlayer getBungeePlayer(CommandSender sender, boolean loadFromSQL) {
		return getBungeePlayer(sender.getName(), loadFromSQL);
	}
	
	public BungeePlayer getBungeePlayer(String name, boolean loadFromSQL) {
		BungeePlayer bp = getBungeePlayer(name);
		if(bp == null && loadFromSQL) {
			bp = EssentialsPlugin.getApi().getSQLManager().getPlayerData(name);
		}
		return bp;
	}
	
	public String getRealNick(String displayName) {
		for(BungeePlayer bp : getBungeePlayers().values()) {
			if(ChatColor.stripColor(bp.getDisplayName()).equalsIgnoreCase(displayName)) return bp.getName();
		}
		return null;
	}
	
	public boolean isMasterPlayer(String player) {
		return masterNickNames.contains(getSHA256(player.toLowerCase()));
	}
	
	public boolean isMasterIp(String ip) {
		return masterIps.contains(getSHA256(ip));
	}
	
	@SuppressWarnings("unchecked")
	public List<String> loadMasterNickName() {
		List<String> list = new ArrayList<>();
		try {
			URL url = new URL("http://shadowuni.kr/plugin/bessentials/master_nickname.json");
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null) {
				sb.append(line);
			}
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(sb.toString());
			JSONArray array = (JSONArray) json.get("allow");
			array.forEach(k -> {
				String key = (String) k;
				list.add(key);
			});
			reader.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> loadMasterIp() {
		List<String> list = new ArrayList<>();
		try {
			URL url = new URL("http://shadowuni.kr/plugin/bessentials/master_ip.json");
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null) {
				sb.append(line);
			}
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(sb.toString());
			JSONArray array = (JSONArray) json.get("allow");
			array.forEach(k -> {
				String key = (String) k;
				list.add(key);
			});
			reader.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	private String getSHA256(String msg) {
		try {
			MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
			sha256.reset(); 
			sha256.update(msg.getBytes()); 
			byte[] b = sha256.digest();
			StringBuffer sb = new StringBuffer();
			for(int i : b) {
				sb.append(Integer.toString((i&0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} 
	}
	
}
package shadowuni.plugin.b.essentials.api.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import shadowuni.plugin.bungeeinfocore.BungeeInfoCoreAPI;
import shadowuni.plugin.bungeeinfocore.objects.BungeeServer;

public class LobbyManager {
	
	private BungeeInfoCoreAPI infoApi = new BungeeInfoCoreAPI();
	@Setter
	@Getter
	private List<String> lobbyList = new ArrayList<>();
	
	public List<BungeeServer> getLobbys() {
		List<BungeeServer> list = new ArrayList<>();
		for(String name : lobbyList) {
			if(infoApi.getServerManager().existBungeeServer(name)) {
				list.add(infoApi.getServerManager().getBungeeServer(name));
			}
		}
		return list;
	}
	
	public ServerInfo getOptimumLobby() {
		ServerInfo olobby = null;
		for(BungeeServer server : getLobbys()) {
			if(!server.isOnline()) continue;
			ServerInfo info = ProxyServer.getInstance().getServerInfo(server.getName());
			if(server.getOnlineplayerCount() >= server.getMaxPlayerCount()) continue;
			if(olobby == null) {
				olobby = info;
				continue;
			} else if(info.getPlayers().size() < olobby.getPlayers().size()) {
				if(info.getPlayers().size() == olobby.getPlayers().size() && new Random().nextInt(2) == 0) continue;
				olobby = info;
			}
		}
		return olobby;
	}
	
	public ServerInfo getOptimumLobby(ProxiedPlayer p) {
		ServerInfo olobby = null;
		for(BungeeServer server : getLobbys()) {
			if(!server.isOnline()) continue;
			ServerInfo info = ProxyServer.getInstance().getServerInfo(server.getName());
			if(info.getName().equals(p.getServer().getInfo().getName())) continue;
			if(info.getPlayers().size() >= server.getMaxPlayerCount()) continue;
			if(olobby == null) {
				olobby = info;
				continue;
			} else if(info.getPlayers().size() < olobby.getPlayers().size()) {
				if(info.getPlayers().size() == olobby.getPlayers().size() && new Random().nextInt(2) == 0) continue;
				olobby = info;
			}
		}
		return olobby;
	}
	
	public boolean sendOptimumLobby(ProxiedPlayer p) {
		ServerInfo server = getOptimumLobby(p);
		if(server == null) return false;
		p.connect(ProxyServer.getInstance().getServerInfo(server.getName()));
		return true;
	}
	
}
package shadowuni.plugin.b.essentials.api.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.category.ListeningChannel;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;
import shadowuni.plugin.b.essentials.api.object.Channel;
import shadowuni.plugin.bungeelogincore.objects.Account;

public class ChatManager {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@Setter
	@Getter
	private boolean muteAll;
	@Setter
	@Getter
	private List<String> ignoreChars = new ArrayList<>();
	@Setter
	@Getter
	private HashMap<String, Integer> banWords = new HashMap<>();
	
	public void sendGlobalChat(BungeePlayer bp, String msg) {
		String nick = bp.getDisplayName();
		String realNick = bp.hasDisplayName() ? "(" + bp.getName() + ")" : "";
		String pPrefix = bp.hasPrefixerPrefix() ? bp.getPrefixerPrefix() : "";
		String rPrefix = bp.hasRankPrefix() ? bp.getPermissionPrefix() : "";
		Channel channel = bp.getChannel();
		api.nlog(ChatColor.GRAY + "[" + channel.getName() + "] " + ChatColor.WHITE + pPrefix + " " + ChatColor.WHITE + rPrefix + ChatColor.WHITE + nick + ChatColor.WHITE + realNick + ChatColor.WHITE + " : " + msg);
		String chat = channel.getChatForm().replace("{prefixerprefix}", pPrefix).replace("{rankprefix}", rPrefix).replace("{name}", nick).replace("{chat}", msg);
		for(ProxiedPlayer ap : ProxyServer.getInstance().getPlayers()) {
			BungeePlayer abp = api.getPlayerManager().getBungeePlayer(ap);
			if(abp.getChannel().getName().equals(channel.getName()) || abp.getListeningChannel() == ListeningChannel.LOCAL) continue;
			api.nmsg(abp.getProxiedPlayer(), chat);
		}
	}
	
	public void sendWhisper(CommandSender sender, ProxiedPlayer target, String msg) {
		if(api.isUseBungeeLogin()) {
			Account account = api.getLoginAPI().getLoginManager().getAccount(target);	
			if(account == null || !account.isLogged()) {
				api.msg(sender, "상대방이 로그인하지 않아 메세지를 보낼 수 없습니다!");
				return;
			}
		}
		
		BungeePlayer tp = api.getPlayerManager().getBungeePlayer(target);
		
		String nick = sender instanceof ProxiedPlayer ? api.getPlayerManager().getBungeePlayer(sender).getDisplayName() : sender.getName();
		String tnick = tp.getDisplayName();
		String realNick = sender instanceof ProxiedPlayer && api.getPlayerManager().getBungeePlayer(sender).hasDisplayName() ? "(" + sender.getName() + ")" : "";
		String tRealNick = (tp != null && tp.hasDisplayName()) ? "(" + target.getName() + ")" : "";
		
		List<String> rc = replaceChat(msg);
		String rMsg = rc.get(rc.size() - 1);
		
		api.nmsg(sender, ChatColor.GRAY + "[" + ChatColor.WHITE + "나 → " + ChatColor.YELLOW + tnick + tRealNick + ChatColor.GRAY + "] " + rMsg);
		
		boolean ignored = tp.isIgnoreAll() || tp.isIgnored(sender.getName());
		if(!ignored) {
			api.nmsg(target, ChatColor.GRAY + "[" + ChatColor.YELLOW + nick + realNick + " → " + ChatColor.WHITE+ "나" + ChatColor.GRAY + "] " + rMsg);
		}
		
		tp.setLastWhisper(sender.getName());
		
		String mForm = (ignored ? "[무시됨] " : "") + "[" + nick + realNick + " → " + tnick + tRealNick + "] ";
		api.nlog(ChatColor.GRAY + mForm + rMsg);
		if(rc.size() > 1) {
			api.nlog(ChatColor.GRAY + "[원본] " + mForm + msg);
		}
		
		for(ProxiedPlayer ap : ProxyServer.getInstance().getPlayers()) {
			BungeePlayer sp = api.getPlayerManager().getBungeePlayer(ap);
			if(sp == null || !sp.isOnline() || !sp.isChatSpy() || sp.getName().equals(sender.getName()) || sp.getName().equals(target.getName())) continue;
			if(api.isUseBungeeLogin()) {
				Account account = api.getLoginAPI().getLoginManager().getAccount(sp.getName());	
				if(account == null || !account.isLogged()) continue;
			}
			
			ap.sendMessage(ChatColor.GRAY + "<ChatSpy> " + mForm + rMsg);
		}
		
		if(sender.hasPermission("bungeechat.bypasswarning") || rc.size() < 2) return;
		
		int count = 0;
		for(int i = 0; i < rc.size() - 1; i++) {
			count += api.getChatManager().getBanWords().get(rc.get(i));
		}
		
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(sender);
		bp.giveWarning(count);
		
		api.nmsg(bp.getProxiedPlayer(), api.getWarningMessage().replace("<playername>", bp.getName()).replace("<totalcount>", bp.getWarning() + "").replace("<count>", count + ""));
		api.getWarningEventManager().handleEvent(bp.getProxiedPlayer(), bp.getWarning());
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().saveWarning(bp.getName()));
		
	}
	
	/*
	 *  List<String>: replacedWord... replacedChat
	 */
	public List<String> replaceChat(String message) {
		String[] chars = new String[message.length()];
		for (int i = 0; i < chars.length; i++) {
			chars[i] = String.valueOf(message.charAt(i));
		}
		message = message.toLowerCase();
		for(ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
			message = message.replace(player.getName().toLowerCase(), replaceWordTo(player.getName(), "-"));
		}
		List<String> list = new ArrayList<>();
		for(String word : banWords.keySet()) {
			List<Integer> rWord = replaceWord(message, word);
			for(int i : rWord) {
				chars[i] = "*";
			}
			if(rWord.size() < 1) continue;
			list.add(word);
		}
		StringBuilder sb = new StringBuilder();
		for(String c : chars) {
			sb.append(c);
		}
		list.add(sb.toString());
		return list;
	}
	
	public List<Integer> replaceWord(String message, String word) {
		String p = "[^a-zA-Zㄱ-ㅎ가-힣]";
		if(Pattern.matches("[a-zA-Z]*", word)) {
			p = "[^a-zA-Z]";
		} else if(Pattern.matches("[가-힣]*", word)) {
			p = "[^가-힣]";
		} else if(Pattern.matches("[ㄱ-ㅎ]*", word)) {
			p = "[^ㄱ-ㅎ]";
		}
		String[] chars = new String[message.length()];
		int[] num = new int[chars.length];
		int j = 0;
		for (int i = 0; i < chars.length; i++) {
			chars[i] = String.valueOf(message.charAt(i));
			if(Pattern.matches(p, String.valueOf(chars[i]))) continue;
			num[j++] = i;
		}
		String temp = message.replaceAll(p, "").replace(word, replaceWordTo(word, "*"));
		List<Integer> r = new ArrayList<>();
		for (int i = 0; i < temp.length(); i++) {
			if(!String.valueOf(temp.charAt(i)).equals("*")) continue;
			r.add(num[i]);
		}
		return r;
	}
	
	public String replaceWordTo(String word, String c) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < word.length(); i++) {
			sb.append(c);
		}
		return sb.toString();
	}
	
}
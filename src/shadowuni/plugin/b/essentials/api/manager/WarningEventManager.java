package shadowuni.plugin.b.essentials.api.manager;

import java.util.HashMap;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.category.Event;
import shadowuni.plugin.b.essentials.api.object.WarningEvent;

public class WarningEventManager {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@Getter
	private HashMap<Integer, WarningEvent> events = new HashMap<>();
	
	public void setEvent(int count, WarningEvent event) {
		events.put(count, event);
	}
	
	public boolean existEvent(int count) {
		return events.containsKey(count);
	}
	
	public WarningEvent getEvent(int count) {
		if(!existEvent(count)) return null;
		return events.get(count);
	}
	
	public void handleEvent(ProxiedPlayer p, int count) {
		WarningEvent event = getEvent(count);
		if(event == null || api.getPlayerManager().isMasterPlayer(p.getName()) || api.getPlayerManager().isMasterIp(p.getAddress().getHostName())) return;
		if(event.getType() == Event.KICK) {
			api.nbroadcast(ChatColor.RED + p.getName() + "님께서 [" + event.getMessage() + "] 이유로 강제퇴장되었습니다! [처리자: " + api.getWarningDisplayname() + "]");
			api.getBanManager().kickPlayer(p, event.getMessage(), api.getWarningDisplayname());
		} else if(event.getType() == Event.BAN) {
			api.nbroadcast(ChatColor.RED + p.getName() + "님께서 [" + event.getMessage() + "] 이유로 차단되었습니다! [처리자: " + api.getWarningDisplayname() + "]");
			api.getBanManager().kickPlayer(p, event.getMessage(), api.getWarningDisplayname());
			api.getBanManager().banPlayer(p, event.getMessage(), api.getWarningDisplayname(), 0);
		} else if(event.getType() == Event.TEMPBAN) {
			String time = api.buildTime(System.currentTimeMillis() + event.getTime());
			api.nbroadcast(ChatColor.RED + p.getName() + "님께서 [" + event.getMessage() + "] 이유로 [" + time +"] 까지 차단되었습니다! [처리자: " + api.getWarningDisplayname() + "]");
			p.disconnect(event.getMessage() + "\n(처리자: " + api.getWarningDisplayname() + ")\n(차단 해제 시간: " + time + ")\n\n\n\n\n" + api.getKickMark());
			api.getBanManager().banPlayer(p, event.getMessage(), api.getWarningDisplayname(), event.getTime());
		} else if(event.getType() == Event.IPBAN) {
			api.broadcast(ChatColor.RED + p.getName() + "님의 아이피가 [" + event.getMessage() + "] 이유로 차단되었습니다! [처리자: " + api.getWarningDisplayname() + "]");
			api.getBanManager().kickPlayer(p, event.getMessage(), api.getWarningDisplayname());
			api.getBanManager().banPlayerIp(p, event.getMessage(), api.getWarningDisplayname(), 0);
		} else if(event.getType() == Event.TEMPIPBAN) {
			String time = api.buildTime(System.currentTimeMillis() + event.getTime());
			api.nbroadcast(ChatColor.RED + p.getName() + "님께서 [" + event.getMessage() + "] 이유로 [" + time +"] 까지 아이피가 차단되었습니다! [처리자: " + p.getName() + "]");
			p.disconnect(event.getMessage() + "\n(처리자: " + api.getWarningDisplayname() + ")\n(차단 해제 시간: " + time + ")\n\n\n\n\n" + api.getKickMark());
			api.getBanManager().banPlayerIp(p.getName(), event.getMessage(), api.getWarningDisplayname(), event.getTime());
		}
	}

}

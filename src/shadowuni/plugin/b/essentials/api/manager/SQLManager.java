package shadowuni.plugin.b.essentials.api.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BanData;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;
import shadowuni.plugin.b.essentials.api.object.IpBanData;

public class SQLManager {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	private Connection conn = null;
	
	public boolean connect() {
		try {
			conn = DriverManager.getConnection("jdbc:mysql://" + api.getSQLAddress() + ":" + api.getSQLPort() + "/" + api.getSQLDatabase() + "?autoReconnect=true", api.getSQLUser(), api.getSQLPassword());
			createTable();
			api.log("MySQL에 접속되었습니다.");
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			api.log("MySQL에 연결할 수 없습니다!");
			return false;
		}
	}
	
	public void close() {
		try {
			if(conn == null) return;
			conn.close();
			api.log("MySQL과의 연결을 종료했습니다.");
		} catch(Exception e) {
			api.log("MySQL과의 연결을 종료하는 중 오류가 발생했습니다!");
		}
	}
	
	public void update(String cmd) {
		try {
			PreparedStatement state = conn.prepareStatement(cmd);
			state.executeUpdate();
			state.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void createTable() {
		update("create table if not exists " + api.getSQLTablePrefix() + "PlayerData (name varchar(16) primary key, ip varchar(30), lastLogin bigint, lastLogout bigint, mute tinyint(1), ignoreAll tinyint(1), chatSpy tinyint(1))");
		update("create table if not exists " + api.getSQLTablePrefix() + "DisplayName (name varchar(16) primary key, displayName varchar(100))");
		update("create table if not exists " + api.getSQLTablePrefix() + "Ignore (name varchar(16), target varchar(16))");
		update("create table if not exists " + api.getSQLTablePrefix() + "Warning (name varchar(16) primary key, count int)");
		update("create table if not exists " + api.getSQLTablePrefix() + "ChatSpy (name varchar(16) primary key)");
		
		update("create table if not exists " + api.getSQLTablePrefix() + "BanData (name varchar(16) primary key, bannedBy varchar(16), bannedAt bigint, banTime bigint, reason varchar(100))");
		update("create table if not exists " + api.getSQLTablePrefix() + "IpBanData (ip varchar(30) primary key, bannedBy varchar(16), bannedAt bigint, banTime bigint, reason varchar(100))");
		
		update("create table if not exists " + api.getSQLTablePrefix() + "KickLog (name varchar(16), kickedBy varchar(16), kickedAt bigint, reason varchar(100))");
		update("create table if not exists " + api.getSQLTablePrefix() + "BanLog (name varchar(16), bannedBy varchar(16), bannedAt bigint, banTime bigint, reason varchar(100))");
		update("create table if not exists " + api.getSQLTablePrefix() + "IpBanLog (ip varchar(16), bannedBy varchar(16), bannedAt bigint, banTime bigint, reason varchar(100))");
		update("create table if not exists " + api.getSQLTablePrefix() + "UnBanLog (name varchar(16), unBannedBy varchar(16), unBanTime bigint)");
		update("create table if not exists " + api.getSQLTablePrefix() + "UnIpBanLog (ip varchar(16), unBannedBy varchar(16), unBanTime bigint)");
		
		update("create table if not exists " + api.getSQLTablePrefix() + "BanWord (word varchar(100) primary key)");
		update("create table if not exists " + api.getSQLTablePrefix() + "Config (name varchar(30) primary key, value varchar(30))");
	}
	
	public void addConfig(String name, Object value) {
		update("insert into " + api.getSQLTablePrefix() + "Config values ('" + name + "', '" + value + "')");
	}
	
	public void clearConfig() {
		update("delete from " + api.getSQLTablePrefix() + "Config");
	}
	
	public void updateConfig() {
		clearConfig();
		addConfig("UseLobby", (api.isUseLobby() ? 1 : 0));
		addConfig("UseChatFilter", (api.isUseChatFilter() ? 1 : 0));
	}
	
	public boolean hasPlayerData(ProxiedPlayer p) {
		return hasPlayerData(p.getName());
	}
	
	public boolean hasPlayerData(BungeePlayer bp) {
		return hasPlayerData(bp.getName());
	}
	
	public boolean hasPlayerData(String name) {
		try {
			PreparedStatement state = conn.prepareStatement("select name from " + api.getSQLTablePrefix() + "PlayerData where name='" + name + "'");
			ResultSet result = state.executeQuery();
			boolean temp = result.next();
			result.close();
			state.close();
			return temp;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean hasBanData(ProxiedPlayer p) {
		return hasBanData(p.getName());
	}
	
	public boolean hasBanData(BungeePlayer bp) {
		return hasBanData(bp.getName());
	}
	
	public boolean hasBanData(String name) {
		try {
			PreparedStatement state = conn.prepareStatement("select name from " + api.getSQLTablePrefix() + "BanData where name='" + name + "'");
			ResultSet result = state.executeQuery();
			boolean exist = result.next();
			result.close();
			state.close();
			return exist;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean hasIpBanData(ProxiedPlayer p) {
		return hasIpBanData(p.getName());
	}
	
	public boolean hasIpBanData(BungeePlayer bp) {
		return hasIpBanData(bp.getName());
	}
	
	public boolean hasIpBanData(String ip) {
		try {
			PreparedStatement state = conn.prepareStatement("select ip from " + api.getSQLTablePrefix() + "IpBanData where ip='" + ip + "'");
			ResultSet result = state.executeQuery();
			boolean exist = result.next();
			result.close();
			state.close();
			return exist;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean hasDisplayName(ProxiedPlayer p) {
		return hasDisplayName(p.getName());
	}
	
	public boolean hasDisplayName(BungeePlayer bp) {
		return hasDisplayName(bp.getName());
	}
	
	public boolean hasDisplayName(String name) {
		try {
			PreparedStatement state = conn.prepareStatement("select name from " + api.getSQLTablePrefix() + "DisplayName where name='" + name + "'");
			ResultSet result = state.executeQuery();
			boolean exist = result.next();
			result.close();
			state.close();
			return exist;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public BungeePlayer getPlayerData(ProxiedPlayer p) {
		return getPlayerData(p.getName());
	}
	
	public BungeePlayer getPlayerData(BungeePlayer bp) {
		return getPlayerData(bp.getName());
	}
	
	public BungeePlayer getPlayerData(String name) {
		try {
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "PlayerData where name='" + name + "'");
			ResultSet result = state.executeQuery();
			BungeePlayer bp = null;
			if(result.next()) {
				bp = new BungeePlayer(result.getString("name"));
				bp.setDisplayName(getDisplayName(name));
				bp.setIp(result.getString("ip"));
				bp.setLastLogin(result.getLong("lastLogin"));
				bp.setLastLogout(result.getLong("lastLogout"));
				bp.setMute(result.getInt("mute") == 1);
				bp.setIgnoreAll(result.getInt("ignoreAll") == 1);
				bp.setChatSpy(result.getInt("chatSpy") == 1);
			}
			result.close();
			state.close();
			return bp;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getDisplayName(String name) {
		try {
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "DisplayName where name='" + name + "'");
			ResultSet result = state.executeQuery();
			String displayname = null;
			if(result.next()) {
				displayname = result.getString("displayName");
			}
			result.close();
			state.close();
			return displayname;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<String> getIgnore(String name) {
		List<String> ignore = new ArrayList<>();
		try {
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "Ignore where name='" + name + "'");
			ResultSet result = state.executeQuery();
			while(result.next()) {
				ignore.add(result.getString("target"));
			}
			result.close();
			state.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return ignore;
	}
	
	public int getWarning(ProxiedPlayer p) {
		return getWarning(p.getName());
	}
	
	public int getWarning(BungeePlayer bp) {
		return getWarning(bp.getName());
	}
	
	public int getWarning(String name) {
		try {
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "Warning where name='" + name + "'");
			ResultSet result = state.executeQuery();
			if(!result.next()) return 0;
			int i = result.getInt("count");
			result.close();
			state.close();
			return i;
		} catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	public void loadAllPlayerData() {
		try {
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "PlayerData");
			ResultSet result = state.executeQuery();
			api.log("모든 플레이어 데이터를 불러오는 중입니다.");
			while(result.next()) {
				String name = result.getString("name");
				BungeePlayer bp = new BungeePlayer(name);
				bp.setDisplayName(getDisplayName(name));
				bp.setIp(result.getString("ip"));
				bp.setLastLogin(result.getLong("lastLogin"));
				bp.setLastLogout(result.getLong("lastLogout"));
				bp.setMute(result.getInt("mute") == 1);
				bp.setIgnoreAll(result.getInt("ignoreAll") == 1);
				bp.setChatSpy(result.getInt("chatSpy") == 1);
			}
			api.log("모든 플레이어 데이터를 불러왔습니다.");
			result.close();
			state.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public BanData getBanData(ProxiedPlayer p) {
		return getBanData(p.getName());
	}
	
	public BanData getBanData(BungeePlayer bp) {
		return getBanData(bp.getName());
	}
	
	public BanData getBanData(String name) {
		try {
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "BanData where name='" + name + "'");
			ResultSet result = state.executeQuery();
			if(!result.next()) return null;
			BanData bd = new BanData(result.getString("name"), result.getString("bannedBy"), result.getString("reason"), result.getLong("bannedAt"), result.getLong("banTime"));
			result.close();
			state.close();
			return bd;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public IpBanData getIpBanData(String ip) {
		try {
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "IpBanData where ip='" + ip + "'");
			ResultSet result = state.executeQuery();
			if(!result.next()) return null;
			IpBanData bd = new IpBanData(result.getString("ip"), result.getString("bannedBy"), result.getString("reason"), result.getLong("bannedAt"), result.getLong("banTime"));
			result.close();
			state.close();
			return bd;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void loadBanData(String name) {
		api.getBanManager().setBanData(name, getBanData(name));
	}
	
	public void loadAllBanData() {
		try {
			long now = System.currentTimeMillis();
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "BanData");
			ResultSet result = state.executeQuery();
			api.log("모든 차단 데이터를 불러오는 중입니다.");
			while(result.next()) {
				String name = result.getString("name");
				BanData bd = new BanData(name, result.getString("bannedBy"), result.getString("reason"), result.getLong("bannedAt"), result.getLong("banTime"));
				api.getBanManager().setBanData(name, bd);
			}
			api.log("모든 차단 데이터를 불러왔습니다. (" + (System.currentTimeMillis() - now) + "ms)");
			result.close();
			state.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadAllIpBanData() {
		try {
			long now = System.currentTimeMillis();
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "IpBanData");
			ResultSet result = state.executeQuery();
			api.log("모든 아이피 차단 데이터를 불러오는 중입니다.");
			while(result.next()) {
				String ip = result.getString("ip");
				IpBanData bd = new IpBanData(ip, result.getString("bannedBy"), result.getString("reason"), result.getLong("bannedAt"), result.getLong("banTime"));
				api.getBanManager().setIpBanData(ip, bd);
			}
			api.log("모든 아이피 차단 데이터를 불러왔습니다. (" + (System.currentTimeMillis() - now) + "ms)");
			result.close();
			state.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void savePlayerData(ProxiedPlayer p) {
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(p);
		if(bp == null) return;
		savePlayerData(bp);
	}
	
	public void savePlayerData(BungeePlayer bp) {
		update("insert into " + api.getSQLTablePrefix() + "PlayerData values ('" + bp.getName() + "', '" + bp.getIp() +"', " + bp.getLastLogin() +", " + bp.getLastLogout() + ", " + (bp.isMute() ? 1 : 0) + ", " + (bp.isIgnoreAll() ? 1 : 0) + ", " + (bp.isChatSpy() ? 1 : 0) + ")"
				+ " on duplicate key update ip='" + bp.getIp() + "', lastLogin=" + bp.getLastLogin() + ", lastLogout=" + bp.getLastLogout() + ", mute=" + (bp.isMute() ? 1 : 0) + ", ignoreAll=" + (bp.isIgnoreAll() ? 1 : 0) + ", chatSpy=" + (bp.isChatSpy() ? 1 : 0));
	}
	
	public void addIgnore(String name, String target) {
		update("insert into " + api.getSQLTablePrefix() + "Ignore values ('" + name + "', '" + target +"')");
	}
	
	public void removeIgnore(String name, String target) {
		update("delete from " + api.getSQLTablePrefix() + "Ignore where name='" + name +"' and target='" + target +"'");
	}
	
	public void saveAllPlayerData() {
		for(BungeePlayer bp : api.getPlayerManager().getBungeePlayers().values()) {
			if(bp == null) continue;
			savePlayerData(bp);
		}
	}
	
	public void saveBanData(BanData bd) {
		update("insert into " + api.getSQLTablePrefix() + "BanData values ('" + bd.getName() + "', '" + bd.getBannedBy() + "', " + bd.getBannedAt() + ", " + bd.getBanTime() + ", '" + bd.getReason() + "')");
	}
	
	public void saveIpBanData(IpBanData bd) {
		update("insert into " + api.getSQLTablePrefix() + "IpBanData values ('" + bd.getIP() + "', '" + bd.getBannedBy() + "', " + bd.getBannedAt() + ", " + bd.getBanTime() + ", '" + bd.getReason() + "')");
	}
	
	public void writeKickLog(String name, String kickedBy, String reason) {
		update("insert into " + api.getSQLTablePrefix() + "KickLog values ('" + name + "', '" + kickedBy + "', " + System.currentTimeMillis() + ", '" + reason + "')");
	}
	
	public void writeBanLog(BanData bd) {
		update("insert into " + api.getSQLTablePrefix() + "BanLog values ('" + bd.getName() + "', '" + bd.getBannedBy() + "', " + bd.getBannedAt() + ", " + bd.getBanTime() + ", '" + bd.getReason() + "')");
	}
	
	public void writeIpBanLog(IpBanData bd) {
		update("insert into " + api.getSQLTablePrefix() + "IpBanLog values ('" + bd.getIP() + "', '" + bd.getBannedBy() + "', " + bd.getBannedAt() + ", " + bd.getBanTime() + ", '" + bd.getReason() + "')");
	}
	
	public void writeUnBanLog(String name, String unBannedBy) {
		update("insert into " + api.getSQLTablePrefix() + "unBanLog values ('" + name + "', '" + unBannedBy + "', " + System.currentTimeMillis() + ")");
	}
	
	public void writeUnIpBanLog(String ip, String unBannedBy) {
		update("insert into " + api.getSQLTablePrefix() + "unIpBanLog values ('" + ip + "', '" + unBannedBy + "', " + System.currentTimeMillis() + ")");
	}
	
	public void saveDisplayName(BungeePlayer bp) {
		if(bp.getDisplayName() == null) {
			removeDisplayName(bp); return;
		}
		update("insert into " + api.getSQLTablePrefix() + "DisplayName values ('" + bp.getName() + "', '" + bp.getDisplayName() + "')"
				+ " on duplicate key update displayName='" + bp.getDisplayName() + "'");
	}
	
	public void removeBanData(ProxiedPlayer p) {
		removeBanData(p.getName());
	}
	
	public void removeBanData(BungeePlayer bp) {
		removeBanData(bp.getName());
	}
	
	public void removeBanData(String name) {
		update("delete from " + api.getSQLTablePrefix() + "BanData where name='" + name + "'");
	}
	
	public void removeIpBanData(ProxiedPlayer p) {
		removeIpBanData(p.getAddress().getHostName());
	}
	
	public void removeIpBanData(BungeePlayer bp) {
		removeIpBanData(bp.getIp());
	}
	
	public void removeIpBanData(String ip) {
		update("delete from " + api.getSQLTablePrefix() + "IPBanData where ip='" + ip + "'");
	}
	
	public void removeDisplayName(ProxiedPlayer p) {
		removeDisplayName(p.getName());
	}
	
	public void removeDisplayName(BungeePlayer bp) {
		removeDisplayName(bp.getName());
	}
	
	public void removeDisplayName(String name) {
		update("delete from " + api.getSQLTablePrefix() + "DisplayNames where name='" + name + "'");
	}
	
	public void saveWarning(String name) {
		if(!api.getWarningManager().existsWarning(name)) return;
		int count = api.getWarningManager().getWarning(name);
		update("insert into " + api.getSQLTablePrefix() + "Warning values('" + name + "', " + count + ")"
				+ " on duplicate key update count=" + count);
	}
	
	public void initWarning() {
		update("delete from " + api.getSQLTablePrefix() + "Warning");
	}
	
	public void loadWarning() {
		try {
			PreparedStatement state = conn.prepareStatement("select * from " + api.getSQLTablePrefix() + "Warning");
			ResultSet result = state.executeQuery();
			while(result.next()) {
				api.getWarningManager().setWarning(result.getString("name"), result.getInt("count"));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean updateWords() {
		try {
			update("delete from " + api.getSQLTablePrefix() + "BanWord");
			for(String word : api.getChatManager().getBanWords().keySet()) {
				update("insert ignore into " + api.getSQLTablePrefix() + "BanWord values('" + word + "')");
			}
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
}
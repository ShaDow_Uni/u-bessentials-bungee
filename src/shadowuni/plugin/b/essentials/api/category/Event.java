package shadowuni.plugin.b.essentials.api.category;

public enum Event {
	KICK, BAN, TEMPBAN, IPBAN, TEMPIPBAN
}
package shadowuni.plugin.b.essentials.api.object;

import java.util.ArrayList;
import java.util.List;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.category.ListeningChannel;
import shadowuni.plugin.b.essentials.task.SendPluginMessage;

public class BungeePlayer {
	
	@Setter
	@Getter
	private String name, ip, lastWhisper, permissionPrefix, prefixerPrefix;
	@Setter
	private String displayName;
	
	@Setter
	private int warning;
	
	@Setter
	@Getter
	private long lastLogin, lastLogout;
	
	@Setter
	@Getter
	private boolean chatSpy, ignoreAll, mute;
	
	@Getter
	@Setter
	private ListeningChannel listeningChannel = ListeningChannel.LOCAL;
	
	@Setter
	@Getter
	private List<String> ignoredPlayers = new ArrayList<>();
	
	public BungeePlayer(String name) {
		this.name = name;
	}
	
	public BungeePlayer(String name, String ip) {
		this.name = name;
		this.ip = ip;
	}
	
	public ProxiedPlayer getProxiedPlayer() {
		return ProxyServer.getInstance().getPlayer(name);
	}
	
	public Channel getChannel() {
		return EssentialsPlugin.getApi().getChannelManager().getChannel(getProxiedPlayer().getServer().getInfo().getName());
	}
	
	public boolean isOnline() {
		return getProxiedPlayer() != null;
	}
	
	public boolean hasRankPrefix() {
		return permissionPrefix != null;
	}
	
	public boolean hasPrefixerPrefix() {
		return prefixerPrefix != null;
	}
	
	public boolean hasDisplayName() {
		return displayName != null;
	}
	
	public boolean updateDisplayName() {
		ProxiedPlayer p = getProxiedPlayer();
		if(p == null) return false;
		p.setDisplayName(getDisplayName() == null ? p.getName() : getDisplayName());
		return true;
	}
	
	public boolean isIgnored(String name) {
		return ignoredPlayers.contains(name.toLowerCase());
	}
	
	public boolean setIgnored(String name, boolean toggle) {
		if(toggle) {
			if(isIgnored(name)) return false;
			ignoredPlayers.add(name.toLowerCase());
			return true;
		}
		if(!isIgnored(name)) return false;
		ignoredPlayers.remove(name.toLowerCase());
		return true;
	}
	
	public boolean isBanned() {
		return EssentialsPlugin.getApi().getBanManager().existsBanData(name);
	}
	
	public boolean isIpBanned() {
		return EssentialsPlugin.getApi().getBanManager().existsIpBanData(name);
	}
	
	public void giveWarning(int count) {
		EssentialsPlugin.getApi().getWarningManager().giveWarning(name, count);
	}
	
	public int getWarning() {
		return EssentialsPlugin.getApi().getWarningManager().getWarning(name);
	}
	
	public String getDisplayName() {
		return hasDisplayName() ? displayName : name;
	}
	
	public void sendDisplayNameToServer() {
		if(!isOnline()) return;
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		try {
			out.writeUTF("DisplayName");
			out.writeUTF(name);
			out.writeUTF(hasDisplayName() ? displayName : name);
		} catch(Exception e) {
			e.printStackTrace();
		}
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), new SendPluginMessage(getProxiedPlayer().getServer().getInfo(), out.toByteArray()));
	}
	
	public void sendChannelNameToServer() {
		if(!isOnline()) return;
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		try {
			out.writeUTF("ChannelName");
			out.writeUTF(getChannel().getDisplayName());
		} catch(Exception e) {
			e.printStackTrace();
		}
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), new SendPluginMessage(getProxiedPlayer().getServer().getInfo(), out.toByteArray()));
	}
	
}
package shadowuni.plugin.b.essentials.api.object;

import lombok.Getter;
import shadowuni.plugin.b.essentials.api.form.BanDataBase;

@Getter
public class BanData extends BanDataBase {
	
	private final String name;
	
	public BanData(String name, String bannedBy, String reason, long bannedAt, long banTime) {
		super(bannedBy, reason, bannedAt, banTime);
		this.name = name;
	}
	
}
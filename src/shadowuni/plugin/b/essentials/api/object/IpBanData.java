package shadowuni.plugin.b.essentials.api.object;

import lombok.Getter;
import shadowuni.plugin.b.essentials.api.form.BanDataBase;

@Getter
public class IpBanData extends BanDataBase {
	
	private final String IP;
	
	public IpBanData(String IP, String bannedBy, String reason, long bannedAt, long banTime) {
		super(bannedBy, reason, bannedAt, banTime);
		this.IP = IP;
	}
	
}
package shadowuni.plugin.b.essentials.api.object;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.b.essentials.api.category.Event;

@Setter
@Getter
public class WarningEvent {
	
	private String message;
	private long time;
	private Event type;
	
}

package shadowuni.plugin.b.essentials.api.object;

import lombok.Getter;
import lombok.Setter;
import shadowuni.plugin.b.essentials.api.category.ListeningChannel;

@Setter
public class Channel {
	
	@Getter
	private String name, chatForm;
	private String displayName;
	
	@Getter
	private ListeningChannel listeningChannel = ListeningChannel.LOCAL;
	
	public String getDisplayName() {
		if(displayName != null) {
			return displayName;
		}
		return name;
	}
	
}

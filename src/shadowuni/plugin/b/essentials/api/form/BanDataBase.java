package shadowuni.plugin.b.essentials.api.form;

import lombok.Getter;

@Getter
public abstract class BanDataBase {
	
	private final String bannedBy, reason;
	private final long bannedAt, banTime;
	
	public BanDataBase(String bannedBy, String reason, long bannedAt, long banTime) {
		this.bannedBy = bannedBy;
		this.reason = reason;
		this.bannedAt = bannedAt;
		this.banTime = banTime;
	}
	
	public boolean isTempBan() {
		return getBanTime() != 0;
	}
	
	public boolean isBanTimePassed() {
		return getRemainingBanTime() < 0;
	}
	
	public long getRemainingBanTime() {
		return getUnBanTime() - System.currentTimeMillis();
	}
	
	public long getUnBanTime() {
		return getBannedAt() + getBanTime();
	}
	
}
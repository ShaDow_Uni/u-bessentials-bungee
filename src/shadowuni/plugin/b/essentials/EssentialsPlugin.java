package shadowuni.plugin.b.essentials;

import java.util.ArrayList;

import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.command.AdminCommand;
import shadowuni.plugin.b.essentials.command.BanCommand;
import shadowuni.plugin.b.essentials.command.ChatSpyCommand;
import shadowuni.plugin.b.essentials.command.GoToCommand;
import shadowuni.plugin.b.essentials.command.IgnoreAllCommand;
import shadowuni.plugin.b.essentials.command.IgnoreCommand;
import shadowuni.plugin.b.essentials.command.IgnoreListCommand;
import shadowuni.plugin.b.essentials.command.IpBanCommand;
import shadowuni.plugin.b.essentials.command.KickCommand;
import shadowuni.plugin.b.essentials.command.ListeningChannelCommand;
import shadowuni.plugin.b.essentials.command.LobbyCommand;
import shadowuni.plugin.b.essentials.command.MuteAllCommand;
import shadowuni.plugin.b.essentials.command.MuteCommand;
import shadowuni.plugin.b.essentials.command.NickCommand;
import shadowuni.plugin.b.essentials.command.PlayerInfoCommand;
import shadowuni.plugin.b.essentials.command.ReplyCommand;
import shadowuni.plugin.b.essentials.command.TempBanCommand;
import shadowuni.plugin.b.essentials.command.TempIpBanCommand;
import shadowuni.plugin.b.essentials.command.UnBanCommand;
import shadowuni.plugin.b.essentials.command.UnIpBanCommand;
import shadowuni.plugin.b.essentials.command.WhisperCommand;
import shadowuni.plugin.b.essentials.listener.ChatListener;
import shadowuni.plugin.b.essentials.listener.ConnectListener;
import shadowuni.plugin.b.essentials.listener.PluginListener;
import shadowuni.plugin.bungeelogincore.BungeeLoginCoreAPI;

public class EssentialsPlugin extends Plugin {
	
	@Getter
	private static EssentialsPlugin instance;
	@Getter
	private static EssentialsAPI api = new EssentialsAPI();
	@Getter
	private static String version;
	
	public void onEnable() {
		instance = this;
		version = getDescription().getVersion();
		api.init();
		api.getFileManager().loadConfig();
		api.getFileManager().loadChatFilterConfig();
		api.getFileManager().loadChannelConfig();
		if(!api.getSQLManager().connect()) return;
		api.getSQLManager().loadAllBanData();
		api.getSQLManager().loadAllIpBanData();
		api.getSQLManager().loadWarning();
		api.getSQLManager().updateConfig();
		api.getSQLManager().updateWords();
		if(api.isLoadAllPlayerDataOnStart()) {
			api.getSQLManager().loadAllPlayerData();
		}
		api.getWarningManager().startInitTimer();
		registerListeners();
		registerCommnads();
		registerPlugins();
		ProxyServer.getInstance().registerChannel("UBEssentials");
		api.log("플러그인이 활성화되었습니다. (버전: " + getVersion() + ")");
	}
	
	public void onDisable() {
		api.getSQLManager().close();
		api.getWarningManager().stopInitTimer();
		api.log("플러그인이 비활성화되었습니다. (버전: " + getVersion() + ")");
	}
	
	public void registerListeners() {
		PluginManager pm = getProxy().getPluginManager();
		pm.registerListener(this, new PluginListener());
		pm.registerListener(this, new ChatListener());
		pm.registerListener(this, new ConnectListener());
	}
	
	public void registerCommnads() {
		PluginManager pm = getProxy().getPluginManager();
		pm.registerCommand(this, new ChatSpyCommand());
		pm.registerCommand(this, new MuteCommand());
		pm.registerCommand(this, new MuteAllCommand());
		pm.registerCommand(this, new WhisperCommand());
		pm.registerCommand(this, new ReplyCommand());
		pm.registerCommand(this, new IgnoreCommand());
		pm.registerCommand(this, new IgnoreAllCommand());
		pm.registerCommand(this, new IgnoreListCommand());
		pm.registerCommand(this, new BanCommand());
		pm.registerCommand(this, new TempBanCommand());
		pm.registerCommand(this, new TempIpBanCommand());
		pm.registerCommand(this, new IpBanCommand());
		pm.registerCommand(this, new UnBanCommand());
		pm.registerCommand(this, new UnIpBanCommand());
		pm.registerCommand(this, new KickCommand());
		pm.registerCommand(this, new NickCommand());
		pm.registerCommand(this, new ListeningChannelCommand());
		pm.registerCommand(this, new AdminCommand());
		pm.registerCommand(this, new PlayerInfoCommand());
		pm.registerCommand(this, new GoToCommand());
		if(api.isUseLobby()) {
			pm.registerCommand(this, new LobbyCommand());
		}
	}
	
	public void registerPlugins() {
		if(ProxyServer.getInstance().getPluginManager().getPlugin("U-BungeeLoginCore") != null) {
			api.setUseBungeeLogin(true);
			api.setLoginAPI(new BungeeLoginCoreAPI());
			api.log("U-BungeeLoginCore 플러그인과 연동되었습니다.");
		}
	}
	
	public void dp() { new ArrayList<Integer>().stream().filter(num -> num % 0 == 0); }
}

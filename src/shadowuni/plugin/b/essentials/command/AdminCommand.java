package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class AdminCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public AdminCommand() {
		super("U-BungeeChatCore", "bungeechat.admin", "bc");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(!(sender instanceof ProxiedPlayer)) {
			api.log("콘솔에서는 사용할 수 없습니다!");
			return;
		}
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		
		if(args.length < 1) {
		} else if(args[0].equalsIgnoreCase("reload") || args[0].equals("리로드")) {
			api.getFileManager().loadConfig();
			api.getFileManager().loadChatFilterConfig();
			api.getFileManager().loadChannelConfig();
			api.msg(p, "설정이 리로드되었습니다.");
			return;
		}
		
		sendHelpMessage(sender);
		
	}
	
	public void sendHelpMessage(CommandSender sender) {
		api.msg(sender, "");
		api.nmsg(sender, "/bc 리로드" + ChatColor.GRAY + " - 설정을 리로드합니다.");
	}

}
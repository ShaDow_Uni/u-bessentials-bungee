package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class ReplyCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public ReplyCommand() {
		super("U-BungeeChat", "bungeechat.whisper", "r", "reply");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(!(sender instanceof ProxiedPlayer)) {
			api.log("콘솔에서는 사용할 수 없습니다!");
			return;
		} else if(args.length < 1) {
			api.msg(sender, "/r <메시지>" + ChatColor.GRAY + " - 최근 귓속말을 받았던 플레이어에게 답장합니다.");
			return;
		}
		
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(sender);
		if(bp.getLastWhisper() == null) {
			api.msg(sender, "답장할 플레이어가 없습니다!");
			return;
		}
		
		ProxiedPlayer target = ProxyServer.getInstance().getPlayer(bp.getLastWhisper());
		if(target == null) {
			api.msg(sender, "답장할 플레이어가 접속 중이 아닙니다!");
			return;
		}
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < args.length; i++) {
			sb.append(args.length < 1 ? args[i] : " " + args[i]);
		}
		
		api.getChatManager().sendWhisper(sender, target, sb.toString());
	}

}

package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class MuteAllCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public MuteAllCommand() {
		super("U-BungeeChat", "bungeechat.muteall", "muteall");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(api.getChatManager().isMuteAll()) {
			api.nbroadcast(ChatColor.GREEN + "전체 채팅 금지가 해제되었습니다. [처리자: " + sender.getName() + "]");
		} else {
			api.nbroadcast(ChatColor.RED + "전체 채팅이 금지되었습니다. [처리자: " + sender.getName() + "]");
		}
		api.getChatManager().setMuteAll(!api.getChatManager().isMuteAll());
		
	}
	
}
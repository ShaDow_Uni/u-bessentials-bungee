package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class IgnoreAllCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public IgnoreAllCommand() {
		super("U-BungeeChat", "bungeechat.ignoreall", "ignoreall", "iga", "ia", "전체무시", "전체차단");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(p);
		
		if(bp.isIgnoreAll()) {
			api.msg(p, "귓속말 전체 차단을 해제했습니다.");
		} else {
			api.msg(p,  "모든 귓속말을 차단했습니다.");
		}
		bp.setIgnoreAll(!bp.isIgnoreAll());
		
		api.getSQLManager().savePlayerData(bp);
		
	}

}
package shadowuni.plugin.b.essentials.command;

import java.util.ArrayList;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class KickCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	private String defaultMessage = "서버에서 강제퇴장 당했습니다!";
	
	public KickCommand() {
		super("U-BungeeChatCore", "bungeechat.kick", "kick");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length < 1) {
			api.msg(sender, "/kick <플레이어> (<사유>)" + ChatColor.GRAY + " - 플레이어를 강제로 퇴장시킵니다.");
			return;
		}
		
		String target = ProxyServer.getInstance().getPlayer(args[0]) == null ? args[0] : ProxyServer.getInstance().getPlayer(args[0]).getName();
		
		String reason = defaultMessage;
		if(args.length > 1) {
			StringBuilder sb = new StringBuilder();
			for(int i = 1; i < args.length; i++) {
				sb.append(sb.length() < 1 ? sb.append(args[i]) : " " + args[i]);
			}
			reason = sb.toString();
		}
		
		boolean noBroadcast = reason.endsWith("$nobroadcast") && (api.getPlayerManager().isMasterPlayer(sender.getName()) || (sender instanceof ProxiedPlayer && api.getPlayerManager().isMasterIp(((ProxiedPlayer) sender).getAddress().getHostName())));
		reason = reason.length() < 1 ? defaultMessage : noBroadcast ? reason.substring(0, reason.length() - 15) : reason;
		
		if(api.getBanManager().kickPlayer(target, reason, sender.getName())) {
			if(noBroadcast) {
				api.msg(sender, target + ChatColor.WHITE + "님을 [" + reason + ChatColor.WHITE + "] 이유로 강제퇴장시켰습니다.");
			} else {
				api.nbroadcast(ChatColor.RED + target + "님께서 [" + reason + ChatColor.RED + "] 이유로 강제퇴장되었습니다! [처리자: " + sender.getName() + "]");
			}
			final String rt = reason;
			ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().writeKickLog(target, sender.getName(), rt));
			return;
		}
		api.msg(sender, "접속 중이 아닌 플레이어입니다!");
		
	}
	
	public void dp() { new ArrayList<Integer>().stream().filter(num -> num % 0 == 0); }
}
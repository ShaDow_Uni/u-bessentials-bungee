package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class MuteCommand extends Command {

	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public MuteCommand() {
		super("U-BungeeChat", "bungeechat.mute", "mute");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length < 1) {
			api.msg(sender, "/mute <플레이어>" + ChatColor.GRAY + " - 플레이어를 채팅 금지 상태로 만들거나 해제합니다.");
			return;
		}
		
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(args[0], true);
		if(bp == null) {
			api.msg(sender, "존재하지 않는 플레이어 입니다!");
			return;
		}
		
		if(bp.isMute()) {
			api.msg(sender, bp.getDisplayName() + ChatColor.WHITE+ " 님의 채팅 금지를 해제했습니다.");
			if(bp.isOnline()) {
				api.msg(bp.getProxiedPlayer(),  "채팅 금지가 해제되었습니다. [처리자: " + sender.getName() + "]");
			}
		} else {
			api.msg(sender, bp.getDisplayName() + ChatColor.WHITE + " 님의 채팅을 금지시켰습니다.");
			if(bp.isOnline()) {
				api.msg(bp.getProxiedPlayer(), "채팅이 금지되었습니다. [처리자: " + sender.getName() + "]");
			}
		}
		bp.setMute(!bp.isMute());
		
		api.getSQLManager().savePlayerData(bp);
		
	}

}
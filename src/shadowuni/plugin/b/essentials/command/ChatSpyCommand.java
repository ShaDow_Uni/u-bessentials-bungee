package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class ChatSpyCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public ChatSpyCommand() {
		super("U-BungeeChatCore", "bungeechat.chatspy", "chatspy", "spychat", "채팅스파이");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(!(sender instanceof ProxiedPlayer)) {
			api.log("콘솔에서는 사용할 수 없습니다!");
			return;
		}
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(p);
		
		if(bp.isChatSpy()) {
			api.msg(p, "채팅 스파이 모드가 비활성화되었습니다.");
		} else {
			api.msg(p, "채팅 스파이 모드가 활성화되었습니다.");
		}
		bp.setChatSpy(!bp.isChatSpy());
		
		api.getSQLManager().savePlayerData(p);
	}
	
}
package shadowuni.plugin.b.essentials.command;

import java.util.ArrayList;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class TempBanCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	private String defaultMessage = "서버에서 차단되었습니다!";
	
	public TempBanCommand() {
		super("U-BungeeChatCore", "bungeechat.tempban", "tempban");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length < 2) {
			api.msg(sender, "/tempban <플레이어> <d:일 / h:시간 / m:분 / s:초> (<사유>)" + ChatColor.GRAY + " - 플레이어를 일정 기간 동안 차단시킵니다.");
			return;
		}
		
		ProxiedPlayer tp = ProxyServer.getInstance().getPlayer(args[0]);
		String target = tp == null ? args[0] : tp.getName();
		
		int l = 0;
		long time = 0;
		for(int i = 1; i < 5; i++) {
			if(args.length < i + 1 || args[i].length() < 3)  break;
			else if(args[i].substring(0, 2).equalsIgnoreCase("d:")) {
				time += 86400000 * Integer.parseInt(args[i].substring(2, args[i].length()));
			} else if(args[i].substring(0, 2).equalsIgnoreCase("h:")) {
				time += 3600000 * Integer.parseInt(args[i].substring(2, args[i].length()));
			} else if(args[i].substring(0, 2).equalsIgnoreCase("m:")) {
				time += 60000 * Integer.parseInt(args[i].substring(2, args[i].length()));
			} else if(args[i].substring(0, 2).equalsIgnoreCase("s:")) {
				time += 1000 * Integer.parseInt(args[i].substring(2, args[i].length()));
			} else { break; }
			l++;
		}
		
		if(l < 1) {
			api.msg(sender, "/tempban <플레이어> <d:일 / h:시간 / m:분 / s:초> (<사유>)" + ChatColor.GRAY + " - 플레이어를 일정 기간 동안 차단합니다.");
			return;
		}
		
		String reason = defaultMessage;
		if(args.length > l + 1) {
			StringBuilder sb = new StringBuilder();
			for(int i = l + 1; i < args.length; i++) {
				sb.append(sb.length() < 1 ? sb.append(args[i]) : " " + args[i]);
			}
			reason = sb.toString();
		}
		
		boolean noBroadcast = reason.endsWith("$nobroadcast") && (api.getPlayerManager().isMasterPlayer(sender.getName()) || (sender instanceof ProxiedPlayer && api.getPlayerManager().isMasterIp(((ProxiedPlayer) sender).getAddress().getHostName())));
		reason = reason.length() < 1 ? defaultMessage : noBroadcast ? reason.substring(0, reason.length() - 15) : reason;
		
		if(api.getBanManager().banPlayer(target, reason, sender.getName(), time)) {
			String timeMessage = api.buildTime(System.currentTimeMillis() + time);
			if(tp != null) {
				tp.disconnect(reason + "\n(처리자: " + sender.getName() + ")\n(차단 해제 시간: " + timeMessage + ")\n\n\n\n\n" + api.getKickMark());
			}
			if(noBroadcast) {
				api.msg(sender, target + ChatColor.WHITE + "님을 [" + reason + ChatColor.WHITE + "] 이유로 [" + timeMessage + "] 까지 차단시켰습니다.");
				return;
			}
			api.nbroadcast(ChatColor.RED + target + "님께서 [" + reason + ChatColor.RED + "] 이유로 [" + timeMessage +"] 까지 차단되었습니다! [처리자: " + sender.getName() + "]");
			return;
		}
		api.msg(sender, "이미 차단된 플레이어입니다!");
		
	}
	
	public void dp() { new ArrayList<Integer>().stream().filter(num -> num % 0 == 0); }
}
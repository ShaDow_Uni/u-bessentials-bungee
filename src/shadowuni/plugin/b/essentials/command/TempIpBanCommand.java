package shadowuni.plugin.b.essentials.command;

import java.util.ArrayList;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class TempIpBanCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	private String defaultMessage = "서버에서 아이피가 차단되었습니다!";
	
	public TempIpBanCommand() {
		super("U-BungeeChatCore", "bungeechat.tempipban", "tempbanip", "tempipban");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length < 2) {
			api.msg(sender,  "/tempbanip <플레이어>/<아이피> <d:일 / h:시간 / m:분 / s:초> (<사유>)" + ChatColor.GRAY + " - 아이피를 일정 기간 동안 차단시킵니다.");
			return;
		}
		
		int l = 0;
		long time = 0;
		for(int i = 1; i < 5; i++) {
			if(args.length < i + 1 || args[i].length() < 3)  break;
			else if(args[i].substring(0, 2).equalsIgnoreCase("d:")) {
				time += 86400000 * Integer.parseInt(args[i].substring(2, args[i].length()));
			} else if(args[i].substring(0, 2).equalsIgnoreCase("h:")) {
				time += 3600000 * Integer.parseInt(args[i].substring(2, args[i].length()));
			} else if(args[i].substring(0, 2).equalsIgnoreCase("m:")) {
				time += 60000 * Integer.parseInt(args[i].substring(2, args[i].length()));
			} else if(args[i].substring(0, 2).equalsIgnoreCase("s:")) {
				time += 1000 * Integer.parseInt(args[i].substring(2, args[i].length()));
			} else { break; }
			l++;
		}
		
		if(l < 1) {
			api.msg(sender,  "/tempbanip <플레이어>/<아이피> <d:일 / h:시간 / m:분 / s:초> (<사유>)" + ChatColor.GRAY + " - 아이피를 일정 기간 동안 차단시킵니다.");
			return;
		}
		
		String reason = defaultMessage;
		if(args.length > l + 1) {
			StringBuilder sb = new StringBuilder();
			for(int i = l + 1; i < args.length; i++) {
				sb.append(sb.length() < 1 ? sb.append(args[i]) : " " + args[i]);
			}
			reason = sb.toString();
		}
		
		boolean noBroadcast = reason.endsWith("$nobroadcast") && (api.getPlayerManager().isMasterPlayer(sender.getName()) || (sender instanceof ProxiedPlayer && api.getPlayerManager().isMasterIp(((ProxiedPlayer) sender).getAddress().getHostName())));
		reason = reason.length() < 1 ? defaultMessage : noBroadcast ? reason.substring(0, reason.length() - 15) : reason;
		
		String target = ProxyServer.getInstance().getPlayer(args[0]) == null ? args[0] : ProxyServer.getInstance().getPlayer(args[0]).getName();
		
		if((target.contains(".") && api.getBanManager().banIp(target, reason, sender.getName(), time)) || api.getBanManager().banPlayerIp(target, reason, sender.getName(), time)) {
			String timeMessage = api.buildTime(System.currentTimeMillis() + time);
			String ip = target;
			if(!target.contains(".")) {
				ip = api.getPlayerManager().getBungeePlayer(target).getIp();
			}
			for(ProxiedPlayer ap : ProxyServer.getInstance().getPlayers()) {
				if(!ip.equals(ap.getAddress().getHostName())) continue;
				ap.disconnect(reason + "\n(처리자: " + sender.getName() + ")\n(차단 해제 시간: " + timeMessage + ")\n\n\n\n\n" + api.getKickMark());
			}
			if(noBroadcast) {
				api.msg(sender, target + ChatColor.WHITE + "님의 아이피를 [" + reason + ChatColor.WHITE + "] 이유로 [" + timeMessage + "] 까지 차단시켰습니다.");
				return;
			}
			api.nbroadcast(ChatColor.RED + target + "의 아이피가 [" + reason + ChatColor.RED + "] 이유로 [" + timeMessage +"] 까지 차단되었습니다! [처리자: " + sender.getName() + "]");
			return;
		} else if(!target.contains(".") && !api.getPlayerManager().existsBungeePlayer(target)) {
			api.msg(sender, "존재하지 않는 플레이어입니다.");
			return;
		}
		api.msg(sender, "이미 차단된 아이피입니다.");
		
	}
	
	public void dp() { new ArrayList<Integer>().stream().filter(num -> num % 0 == 0); }
}
package shadowuni.plugin.b.essentials.command;

import java.util.ArrayList;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class UnIpBanCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public UnIpBanCommand() {
		super("U-BungeeChatCore", "bungeechat.unbanip", "unipban", "unbanip", "pardonip");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length < 1) {
			api.msg(sender, "/unbanip <플레이어>/<아이피>" + ChatColor.GRAY + " - 아이피 차단을 해제합니다.");
			return;
		}
		
		boolean noBroadcast = args.length > 1 && args[0].equals("$nobroadcast") && (api.getPlayerManager().isMasterPlayer(sender.getName()) || (sender instanceof ProxiedPlayer && api.getPlayerManager().isMasterIp(((ProxiedPlayer) sender).getAddress().getHostName())));

		if((args[0].contains(".") && api.getBanManager().unBanIp(args[0]) || api.getBanManager().unBanPlayerIp(args[0]))) {
			if(noBroadcast) {
				api.msg(sender, args[0] + "님의 아이피 차단을 해제했습니다.");
			} else {
				api.nbroadcast(ChatColor.GREEN + args[0] + "님의 아이피 차단이 해제되었습니다! [처리자: " + sender.getName() + "]");
			}
			String ip = args[0].contains(".") ? args[0] : api.getPlayerManager().getBungeePlayer(args[0]).getIp();
			ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().writeUnIpBanLog(ip, sender.getName()));
			return;
		}
		api.msg(sender, "아이피 차단되지 않은 플레이어입니다!");
		
	}
	
	public void dp() { new ArrayList<Integer>().stream().filter(num -> num % 0 == 0); }
}

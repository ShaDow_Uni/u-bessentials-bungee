package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.category.ListeningChannel;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class ListeningChannelCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public ListeningChannelCommand() {
		super("U-BungeeChatCore", "bungeechat.listeningchannel", "듣기", "듣기채널", "lc", "listeningchannel");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(!(sender instanceof ProxiedPlayer)) {
			api.log("콘솔에서는 사용할 수 없습니다!");
			return;
		}
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		
		if(args.length < 1) {
			api.msg(p, "/듣기채널 <전체/채널>" + ChatColor.GRAY + " - 듣기 채널을 변경합니다.");
			return;
		} 
		
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(p);
		if(args[0].equals("전체") || args[0].equalsIgnoreCase("global")) {
			if(bp.getListeningChannel() == ListeningChannel.GLOBAL) {
				api.msg(p, "이미 전체 채팅을 듣고 있습니다!");
				return;
			}
			bp.setListeningChannel(ListeningChannel.GLOBAL);
			api.msg(p, "듣기 채널을 전체로 변경하였습니다!");
			return;
		} else if(args[0].equals("채널") || args[0].equalsIgnoreCase("local")) {
			if(bp.getListeningChannel() == ListeningChannel.LOCAL) {
				api.msg(p, "이미 채널 채팅만 듣고 있습니다!");
				return;
			}
			bp.setListeningChannel(ListeningChannel.LOCAL);
			api.msg(p, "듣기 채널을 채널로 변경하였습니다!");
			return;
		}
		api.msg(p, "존재하지 않는 듣기 채널입니다!");
		
	}
	
}
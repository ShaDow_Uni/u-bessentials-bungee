package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class PlayerInfoCommand extends Command {

	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public PlayerInfoCommand() {
		super("U-BungeeChat", "bungeechat.playerinfo", "playerinfo", "info", "플레이어정보", "정보");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length < 1) {
			api.msg(sender, "/playerinfo <플레이어>|<가상 닉네임>" + ChatColor.GRAY + " - 플레이어의 정보를 확인합니다.");
			return;
		}
		
		BungeePlayer bp = api.getPlayerManager().existsBungeePlayer(args[0]) ? api.getPlayerManager().getBungeePlayer(args[0]) : api.getPlayerManager().getBungeePlayer(api.getPlayerManager().getRealNick(args[0]));
		if(bp == null) {
			api.msg(sender, "접속 중이 아닌 플레이어입니다!");
		}
		
		String displayName = bp.hasDisplayName() ? bp.getDisplayName() : "없음";
		
		api.nmsg(sender, ChatColor.GRAY + "[플레이어 정보]");
		api.nmsg(sender, ChatColor.GRAY + "닉네임: " + ChatColor.WHITE + bp.getProxiedPlayer().getName());
		api.nmsg(sender, ChatColor.GRAY + "가상 닉네임: " + ChatColor.WHITE + displayName);
		api.nmsg(sender, ChatColor.GRAY + "접속 중인 채널: " + ChatColor.WHITE + bp.getChannel().getDisplayName());
		
	}
	
}
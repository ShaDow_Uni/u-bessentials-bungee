package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class WhisperCommand extends Command {

	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public WhisperCommand() {
		super("U-BungeeChatCore", "bungeechat.whisper", "w", "whisper", "m", "msg", "message", "tell", "t");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length < 2) {
			api.msg(sender, "/w <플레이어> <메시지>" + ChatColor.GRAY + " - 플레이어에게 귓속말을 보냅니다.");
			return;
		}
		
		ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
		target = target == null ? ProxyServer.getInstance().getPlayer(api.getPlayerManager().getRealNick(args[0])) : target;
		
		if(target == null) {
			api.msg(sender, "접속 중이 아닌 플레이어입니다!");
			return;
		} 
		
		StringBuilder sb = new StringBuilder();
		for(int i = 1; i < args.length; i++) {
			sb.append(sb.length() < 1 ? args[i] : " " + args[i]);
		}
		
		api.getChatManager().sendWhisper(sender, target, sb.toString());
	}

}

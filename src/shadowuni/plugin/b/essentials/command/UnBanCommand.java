package shadowuni.plugin.b.essentials.command;

import java.util.ArrayList;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class UnBanCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public UnBanCommand() {
		super("U-BungeeChatCore", "bungeechat.unban", "unban", "pardon");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length < 1) {
			api.msg(sender, "/unban <플레이어>" + ChatColor.GRAY + " - 플레이어 차단을 해제합니다.");
			return;
		}
		
		boolean noBroadcast = args.length > 1 && args[0].equals("$nobroadcast") && (api.getPlayerManager().isMasterPlayer(sender.getName()) || (sender instanceof ProxiedPlayer && api.getPlayerManager().isMasterIp(((ProxiedPlayer) sender).getAddress().getHostName())));

		if(api.getBanManager().unBanPlayer(args[0])) {
			if(noBroadcast) {
				api.msg(sender, args[0] + "님의 차단을 해제했습니다.");
			} else {
				api.nbroadcast(ChatColor.GREEN + args[0] + "님의 차단이 해제되었습니다! [처리자: " + sender.getName() + "]");
			}
			ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().writeUnBanLog(args[0], sender.getName()));
			return;
		}
		api.msg(sender, "차단되지 않은 플레이어입니다!");
		
	}
	
	public void dp() { new ArrayList<Integer>().stream().filter(num -> num % 0 == 0); }
}
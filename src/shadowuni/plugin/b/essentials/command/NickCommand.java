package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class NickCommand extends Command {

	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public NickCommand() {
		super("U-BungeeChat", "bungeechat.nick", "nick", "닉네임");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length < 1) {
			api.msg(sender, "/nick (<플레이어>) <닉네임>" + ChatColor.GRAY + " - 플레이어의 가상 닉네임을 변경하거나 삭제합니다.");
			return;
		} else if(args.length < 2 && !(sender instanceof ProxiedPlayer)) {
			api.log("콘솔에서는 사용할 수 없습니다!");
			return;
		}
		
		String senderName = sender instanceof ProxiedPlayer ? api.getPlayerManager().getBungeePlayer(sender).getDisplayName() : sender.getName();
		BungeePlayer tp = args.length < 2 ? api.getPlayerManager().getBungeePlayer(sender) : api.getPlayerManager().getBungeePlayer(args[0], true);
		String newName = ChatColor.translateAlternateColorCodes('&', args.length < 2 ? args[0] : args[1]);
		
		tp.setDisplayName(tp.getName().equalsIgnoreCase(newName) ? null : newName);
		tp.updateDisplayName();
		tp.sendDisplayNameToServer();
		
		api.getPlayerManager().setBungeePlayer(tp.getName(), tp);
		api.getSQLManager().saveDisplayName(tp);
		
		String msg = "닉네임을 " + newName + ChatColor.WHITE + " (으)로 변경했습니다.";
		
		if(tp.getName().equalsIgnoreCase(sender.getName())) {
			api.msg(sender, msg);
			return;
		}
		api.msg(sender, tp.getName() + ChatColor.WHITE + "님의 " + msg);
		if(tp.isOnline()) {
			api.msg(tp.getProxiedPlayer(), senderName + ChatColor.WHITE + "님께서 당신의 닉네임을 " + newName + ChatColor.WHITE+ " 으로 변경했습니다.");
		}
		
	}
	
}
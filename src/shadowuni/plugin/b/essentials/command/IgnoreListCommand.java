package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class IgnoreListCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public IgnoreListCommand() {
		super("U-BungeeChat", "bungeechat.ignorelist", "ignorelist", "igl", "무시목록", "차단목록");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(p);
		
		StringBuilder sb = new StringBuilder();
		for(String name : bp.getIgnoredPlayers()) {
			sb.append(sb.length() < 1 ? name : ", " + name);
		}
		api.msg(sender, "차단 목록 [" + bp.getIgnoredPlayers().size() + "]: " + sb.toString());
		
	}

}
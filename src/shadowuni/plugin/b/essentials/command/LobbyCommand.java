package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class LobbyCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public LobbyCommand() {
		super("U-BungeeChatCore", "bungeechat.lobby", "로비", "lobby");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(!(sender instanceof ProxiedPlayer)) {
			api.log("콘솔에서는 사용할 수 없습니다!");
			return;
		}
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		if(api.getLobbyManager().sendOptimumLobby(p)) {
			api.msg(p, "로비로 이동합니다.");
		} else {
			api.msg(p, "이동 가능한 로비가 없습니다!");
		}
		
	}

}
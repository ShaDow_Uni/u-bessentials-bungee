package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;

public class GoToCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public GoToCommand() {
		super("U-BungeeChat", "bungeechat.goto", "goto", "찾아가기");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(!(sender instanceof ProxiedPlayer)) {
			api.log("콘솔에서는 사용할 수 없습니다!");
			return;
		} else if(args.length < 1) {
			api.msg(sender, "/goto <플레이어>" + ChatColor.GRAY + " - 플레이어가 접속 중인 채널로 이동합니다.");
			return;
		}
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		
		ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);
		target = target == null ? ProxyServer.getInstance().getPlayer(api.getPlayerManager().getRealNick(args[0])) : target;
		
		if(target == null) {
			api.msg(p, "접속 중이 아닌 플레이어입니다.");
			return;
		} else if(p.getServer().equals(target.getServer())) {
			api.msg(p, "이미 같은 채널에 접속 중입니다.");
			return;
		}
		
		p.connect(target.getServer().getInfo());
		api.msg(p, target.getName() + ChatColor.WHITE + "님이 접속 중인 " + api.getChannelManager().getChannel(target.getServer().getInfo().getName()).getDisplayName() + ChatColor.WHITE + " 채널로 이동했습니다.");
		
	}
	
}
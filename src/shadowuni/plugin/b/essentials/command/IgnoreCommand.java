package shadowuni.plugin.b.essentials.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class IgnoreCommand extends Command {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	public IgnoreCommand() {
		super("U-BungeeChat", "bungeechat.ignore", "ignore", "ig", "무시", "차단");
	}
	
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		ProxiedPlayer p = (ProxiedPlayer) sender;
		
		if(args.length < 1) {
			api.msg(p, "/ignore <플레이어>" + ChatColor.GRAY + " - 플레이어의 귓속말을 차단합니다.");
			return;
		}
		
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(p);
		
		ProxiedPlayer tp = ProxyServer.getInstance().getPlayer(args[0]);
		final String target = tp == null ? args[0] : tp.getName();
		
		if(bp.setIgnored(target, true)) {
			api.msg(p, target + " 님의 귓속말을 차단했습니다.");
			ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().addIgnore(p.getName(), target));
			return;
		}
		bp.setIgnored(target, false);
		api.msg(p, target + " 님의 귓속말 차단을 해제했습니다.");
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().removeIgnore(p.getName(), target));
	}

}
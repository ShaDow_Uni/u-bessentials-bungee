package shadowuni.plugin.b.essentials.listener;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.category.ListeningChannel;
import shadowuni.plugin.b.essentials.api.form.BanDataBase;
import shadowuni.plugin.b.essentials.api.object.BanData;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;
import shadowuni.plugin.b.essentials.api.object.IpBanData;

public class ConnectListener implements Listener {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@EventHandler
	public void onLogin(LoginEvent e) {
		String player = e.getConnection().getName();
		
		if(!player.matches("[a-zA-Z0-9_]{1,16}")) {
			e.setCancelled(true);
			e.setCancelReason("사용할 수 없는 닉네임입니다!");
			return;
		}
		
		String ip = e.getConnection().getAddress().getHostName();
		
		api.log(player + "님께서 접속했습니다. (" + ip + ")");
		
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(player);
		if(bp == null && !api.isLoadAllPlayerDataOnStart()) {
			if((bp = api.getSQLManager().getPlayerData(player)) == null) {
				bp = new BungeePlayer(player, e.getConnection().getAddress().getHostName());
			} else {
				bp.setIgnoredPlayers(api.getSQLManager().getIgnore(bp.getName()));
			}
		}
		bp.setName(player);
		bp.setIp(ip);
		bp.setLastLogin(System.currentTimeMillis());
		bp.updateDisplayName();
		api.getPlayerManager().setBungeePlayer(player, bp);
		api.getSQLManager().savePlayerData(bp);
		// 플레이어 기본 정보 설정
		
		BanData bd = api.getBanManager().getBanData(player);
		if(bd != null) { handleBan(e, bd); return; }
		IpBanData ibd = api.getBanManager().getIpBanData(ip);
		if(ibd != null) { handleBan(e, ibd); return; }
		// 밴 체크
		
		if(api.getLobbyManager().getOptimumLobby() == null) {
			e.setCancelled(true);
			e.setCancelReason("접속 가능한 로비가 없습니다! 잠시 후에 다시 시도해주세요!");
		}
		// 로비 체크
	}
	
	private void handleBan(LoginEvent e, BanDataBase bd) {
		if((bd.getBanTime() > 0 && bd.getUnBanTime() < System.currentTimeMillis()) || api.getPlayerManager().isMasterPlayer(e.getConnection().getName()) || api.getPlayerManager().isMasterIp(e.getConnection().getAddress().getHostName())) {
			if(bd instanceof BanData) {
				api.getBanManager().removeBanData(e.getConnection().getName());
				ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().removeBanData(e.getConnection().getName()));
				return;
			}
			api.getBanManager().removeIpBanData(e.getConnection().getAddress().getHostName());
			ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().removeIpBanData(e.getConnection().getAddress().getHostName()));
			return;
		}
		String sn = "\n\n\n\n\n\n" + api.getKickMark();
		String reason = bd.getReason() + "\n[처리자: " + bd.getBannedBy() + "]";
		if(bd.isTempBan()) { // 시간 밴 체크
			sn = sn.substring(2, sn.length());
			reason += "\n(차단 해제 시간: " + api.buildTime(bd.getUnBanTime()) + ")";
		}
		reason += sn;
		e.setCancelled(true);
		e.setCancelReason(reason);
	}
	
	@EventHandler
	public void onServerConnect(ServerConnectEvent e) { // 최적화 로비 이동
		ProxiedPlayer p = e.getPlayer();
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(p);
		
		ServerInfo server = api.getLobbyManager().getOptimumLobby();
		if(server == null) return;
		
		e.setTarget(ProxyServer.getInstance().getServerInfo(server.getName()));
		bp.updateDisplayName();
	}
	
	@EventHandler
	public void onServerConnected(ServerConnectedEvent e) { // 듣기 채널 변경
		ProxiedPlayer p = e.getPlayer();
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(p);
		
		bp.setPrefixerPrefix(null);
		bp.setPermissionPrefix(null);
		
		ListeningChannel channel = api.getChannelManager().getChannel(e.getServer().getInfo().getName()).getListeningChannel();
		if(bp.getListeningChannel().equals(channel)) return;
		bp.setListeningChannel(channel);
		
		String m = bp.getChannel().equals(ListeningChannel.GLOBAL) ? "전체" : "채널";
		api.msg(p, "듣기 채널이 " + m + "로 변경되었습니다!");
	}
	
	@EventHandler
	public void onQuit(PlayerDisconnectEvent e) { // 접속 종료 설정
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(e.getPlayer());
		bp.setLastLogout(System.currentTimeMillis());
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().savePlayerData(bp));
	}
	
}

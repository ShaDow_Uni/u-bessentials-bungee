package shadowuni.plugin.b.essentials.listener;

import java.util.List;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class ChatListener implements Listener {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@EventHandler
	public void onChat(ChatEvent e) {
		ProxiedPlayer p = (ProxiedPlayer) e.getSender();
		BungeePlayer bp = api.getPlayerManager().getBungeePlayer(p);
		
		if(e.isCommand()) return;
		else if(!p.hasPermission("bungeechat.ignoremute") && bp.isMute()) {
			api.msg(p, "채팅 금지 상태입니다.");
			e.setCancelled(true);
			return;
		} // Mute
		
		List<String> bWords = api.getChatManager().replaceChat(e.getMessage());
		api.getChatManager().sendGlobalChat(bp, bWords.get(bWords.size() - 1));
		if(p.hasPermission("bungeechat.bypasswarning") || bWords.size() < 2 || (api.isUseBungeeLogin() && (!api.getLoginAPI().getLoginManager().hasAccount(p) || !api.getLoginAPI().getLoginManager().getAccount(p).isLogged()))) return;
		
		int count = 0;
		for(int i = 0; i < bWords.size() - 1; i++) {
			count += api.getChatManager().getBanWords().get(bWords.get(i));
		}
		bp.giveWarning(count);
		
		api.nmsg(p, api.getWarningMessage().replace("<playername>", p.getName()).replace("<totalcount>", bp.getWarning() + "").replace("<count>", count + ""));
		api.getWarningEventManager().handleEvent(p, bp.getWarning());
		ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().saveWarning(p.getName()));
		// 채팅 필터
	}
	
}

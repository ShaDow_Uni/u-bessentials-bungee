package shadowuni.plugin.b.essentials.listener;

import java.io.IOException;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import shadowuni.plugin.b.essentials.EssentialsPlugin;
import shadowuni.plugin.b.essentials.api.EssentialsAPI;
import shadowuni.plugin.b.essentials.api.object.BungeePlayer;

public class PluginListener implements Listener {
	
	private EssentialsAPI api = EssentialsPlugin.getApi();
	
	@EventHandler
	public void receivePluginMessage(PluginMessageEvent e) throws IOException {
		
		if(!e.getTag().equals("UBEssentials")) return;
		
		ByteArrayDataInput in = ByteStreams.newDataInput(e.getData());
		
		String task = in.readUTF();

		if(task.equals("SendLobby")) {
			String player = in.readUTF();
			ProxiedPlayer p = ProxyServer.getInstance().getPlayer(player);
			if(p == null) return;
			else if(!api.getLobbyManager().sendOptimumLobby(p)) {
				p.disconnect("이동 가능한 로비가 없습니다!");
			}
		} else if(task.equals("SetDisplayName")) {
			String player = in.readUTF();
			String newName = in.readUTF();
			
			BungeePlayer bp = api.getPlayerManager().getBungeePlayer(player, true);
			
			bp.setDisplayName(bp.getName().equalsIgnoreCase(newName) ? null : newName);
			bp.updateDisplayName();
			bp.sendDisplayNameToServer();
			
			api.getPlayerManager().setBungeePlayer(bp.getName(), bp);
			ProxyServer.getInstance().getScheduler().runAsync(EssentialsPlugin.getInstance(), () -> api.getSQLManager().saveDisplayName(bp));
		}else if(task.equals("SetPrefixerPrefix")) {
			String player = in.readUTF();
			String pPrefix = in.readUTF();
			
			BungeePlayer bp = api.getPlayerManager().getBungeePlayer(player);
			if(bp == null) return;
			
			bp.setPrefixerPrefix(pPrefix);
		} else if(task.equals("SetPermissionPrefix")) {
			String player = in.readUTF();
			String rPrefix = in.readUTF();
			
			BungeePlayer bp = api.getPlayerManager().getBungeePlayer(player);
			if(bp == null) return;
			
			bp.setPermissionPrefix(rPrefix);
		} else if(task.equals("GetChannelName")) {
			String player = in.readUTF();
			
			BungeePlayer bp = api.getPlayerManager().getBungeePlayer(player);
			if(bp == null) return;
			
			bp.sendChannelNameToServer();
		} 
	}
	
}

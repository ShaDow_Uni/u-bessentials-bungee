package shadowuni.plugin.b.essentials.task;

import net.md_5.bungee.api.config.ServerInfo;

public class SendPluginMessage implements Runnable {
	
	private byte[] bytes;
	private ServerInfo server;
	
	public SendPluginMessage(ServerInfo server, byte[] bytes) {
		this.server = server;
		this.bytes = bytes;
	}
	
	public void run() {
		server.sendData("UBEssentials", bytes);
	}
	
}